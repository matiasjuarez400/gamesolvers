package mathecat.block.puzzleimagesolver.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ImageIOHandler {
    public static final String DEFAULT_EXTENSION = ".png";
    public static final String DEFAULT_FORMAT = "png";

    public BufferedImage loadImageByUrl(String imageUrl) {
        try {
            return ImageIO.read(new File(imageUrl));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public BufferedImage loadImageByResourceName(String resourceName) {
        try {
            return (ImageIO.read(ImageIOHandler.class.getResource(resourceName)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public BufferedImage loadImageFromFile(File file) {
        return loadImageByUrl(file.getAbsolutePath());
    }

    public void saveImage(BufferedImage bufferedImage, String folder, String imageName) {
        List<String> imageNamePortions = Arrays.asList(folder, imageName + DEFAULT_EXTENSION);
        File imageFile = new File(String.join(File.separator, imageNamePortions));
        imageFile.mkdirs();
        try {
            ImageIO.write((bufferedImage),
                    DEFAULT_FORMAT,
                    imageFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
