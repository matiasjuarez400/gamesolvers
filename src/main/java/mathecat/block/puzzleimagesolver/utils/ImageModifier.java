package mathecat.block.puzzleimagesolver.utils;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.Rectangle;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ImageModifier {
    private void markImageAroundPixel(Point point, int pixelColor, BufferedImage bufferedImage) {
        markImageAroundPixel(point.x, point.y, pixelColor, bufferedImage, 2);
    }

    private void markImageAroundPixel(int x, int y, int pixelColor, BufferedImage bufferedImage) {
        markImageAroundPixel(x, y, pixelColor, bufferedImage, 2);
    }

    private void markImageAroundPixel(int x, int y, int pixelColor, BufferedImage bufferedImage, int markSize) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();

        int desiredSize = markSize;

        int ix = x - desiredSize < 0 ? 0 : x - desiredSize;
        int ex = x + desiredSize < w ? x + desiredSize : w - 1;
        int iy = y - desiredSize < 0 ? 0 : y - desiredSize;
        int ey = y + desiredSize < h ? y + desiredSize : h - 1;

        for (int i = ix; i <= ex; i++) {
            for (int j = iy; j <= ey; j++) {
                bufferedImage.setRGB(i, j, pixelColor);
            }
        }
    }

    private BufferedImage copyImageMethod1(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null).getSubimage(0, 0, bi.getWidth(), bi.getHeight());
    }

    private BufferedImage copyImageMethod2(BufferedImage source){
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics2D g = (Graphics2D) b.getGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return b;
    }

    public BufferedImage copyImage(BufferedImage bufferedImage) {
        return copyImageMethod2(bufferedImage);
    }

    public BufferedImage drawPointsInImage(BufferedImage bufferedImage, List<Point> points) {
        return drawPointsInImage(bufferedImage, points, true);
    }

    public BufferedImage drawPointsInImage(BufferedImage bufferedImage, List<Point> points, boolean createImageCopy) {
        BufferedImage copy = bufferedImage;
        if (createImageCopy) {
            copy = copyImage(bufferedImage);
        }

        for (Point point : points) {
            markImageAroundPixel(point, Color.WHITE.getRGB(), copy);
        }

        return copy;
    }

    public BufferedImage drawPointsInImage(BufferedImage bufferedImage, Map<Integer, List<Point>> pointsMap, boolean createImageCopy) {
        List<Point> allPoints = new ArrayList<>();
        for (List<Point> pointList : pointsMap.values()) {
            allPoints.addAll(pointList);
        }

        return drawPointsInImage(bufferedImage, allPoints, createImageCopy);
    }

    public BufferedImage drawRectangleInImage(BufferedImage image, Rectangle rectangle) {
        return drawRectangleInImage(image, rectangle, true);
    }

    public BufferedImage drawRectangleInImage(BufferedImage image, Rectangle rectangle, boolean copyImage) {
        List<Point> points = new ArrayList<>();

        for (int x = rectangle.getUpperLeftVertex().x; x < rectangle.getUpperRightVertex().x; x++) {
            points.add(new Point(x, rectangle.getUpperLeftVertex().y));
            points.add(new Point(x, rectangle.getLowerLeftVertex().y));
        }

        for (int y = rectangle.getUpperLeftVertex().y; y < rectangle.getLowerLeftVertex().y; y++) {
            points.add(new Point(rectangle.getUpperLeftVertex().x, y));
            points.add(new Point(rectangle.getUpperRightVertex().x, y));
        }

        return drawPointsInImage(image, points, copyImage);
    }

    public BufferedImage drawLineInImage(BufferedImage image, Line line) {
        return drawLineInImage(image, line, true);
    }

    public BufferedImage drawLineInImage(BufferedImage image, Line line, boolean useCopyOfImage) {
        List<Point> points = new ArrayList<>();

        if (line.isHorizontal()) {
            for (int x = line.getStart().x; x < line.getEnd().x; x++) {
                points.add(new Point(x, line.getEnd().y));
            }
        } else if (line.isVertical()) {
            for (int y = line.getStart().y; y < line.getEnd().y; y++) {
                points.add(new Point(line.getEnd().x, y));
            }
        } else {
            throw new IllegalArgumentException("Online horizontal and vertical lines are supported");
        }

        return drawPointsInImage(image, points, useCopyOfImage);
    }
}
