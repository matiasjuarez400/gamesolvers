package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.puzzleimagesolver.model.Pair;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.PositionedPixel;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.BoardSquareSampleAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.BoardSquareSampleResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class BoardSectionAnalyser extends AbstractAnalyser<BoardSectionResult> {
    private BoardSquareSampleResult sampleResult;
    private static final int DISTANCE_AROUND_LINE = 1;

    public BoardSectionAnalyser() {
        sampleResult = new BoardSquareSampleAnalyser().analyse();
    }

    @Override
    public BoardSectionResult analyze(BufferedImage image) {
        BoardSectionResult result = new BoardSectionResult();
        result.setOriginalImage(image);

        launchVerticalSearch(image, result);
        launchHorizontalSearch(image, result);

        return result;
    }

    private void launchVerticalSearch(BufferedImage image, BoardSectionResult result) {
        Line drivingLine = new Line(DISTANCE_AROUND_LINE, 0, DISTANCE_AROUND_LINE, image.getHeight());
        int lineMovements = image.getWidth() - DISTANCE_AROUND_LINE * 2;

        searchBoardSquaresDelimiters(sampleResult.getLeftLimit(), result.getLeftPoints(), image, drivingLine, lineMovements);
        searchBoardSquaresDelimiters(sampleResult.getRightLimit(), result.getRightPoints(), image, drivingLine, lineMovements);
    }

    private void launchHorizontalSearch(BufferedImage image, BoardSectionResult result) {
        Line drivingLine = new Line(0, DISTANCE_AROUND_LINE, image.getWidth(), DISTANCE_AROUND_LINE);
        int lineMovements = image.getHeight() - DISTANCE_AROUND_LINE * 2;

        searchBoardSquaresDelimiters(sampleResult.getUpperLimit(), result.getUpPoints(), image, drivingLine, lineMovements);
        searchBoardSquaresDelimiters(sampleResult.getBottomLimit(), result.getDownPoints(), image, drivingLine, lineMovements);
    }

    private void searchBoardSquaresDelimiters(Range range, List<Point> pointStore, BufferedImage image,
                                              Line drivingLine, int lineMovements) {
        for (int i = 0; i < lineMovements; i++) {
            List<Point> currentPoints = searchRangeOnLine(image, drivingLine, range, DISTANCE_AROUND_LINE);

            if (!currentPoints.isEmpty()) {
                pointStore.addAll(currentPoints);
            }

            if (drivingLine.isVertical()) {
                drivingLine = drivingLine.moveHorizontally(1);
            } else if (drivingLine.isHorizontal()) {
                drivingLine = drivingLine.moveVertically(1);
            } else {
                throw new IllegalStateException("DrivingLine should be horizontal or vertical");
            }
        }
    }

    private List<Point> searchRangeOnLine(BufferedImage image, Line line, Range range, int distanceAroundLine) {
        List<Pair<PositionedPixel, PositionedPixel>> pixelsAroundLine =
                basicImageAnalyser.extractPixelsAroundLine(image, line, distanceAroundLine);

        List<Point> pointsAroundSquares = new ArrayList<>();
        for (Pair<PositionedPixel, PositionedPixel> positionedPixelPair : pixelsAroundLine) {
            double brightnessVariation = super.calculationsHelper.calculateBrightnessVariation(
                    positionedPixelPair.getLeft().getPixel(), positionedPixelPair.getRight().getPixel()
            );

            if(NumericUtils.isNumberInRange(brightnessVariation, range)) {
                Point middlePoint = positionedPixelPair.getLeft().getPosition().calculateMiddlePoint(
                        positionedPixelPair.getRight().getPosition()
                );

                pointsAroundSquares.add(middlePoint);
            }
        }

        return pointsAroundSquares;
    }
}
