package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results;

import java.awt.image.BufferedImage;

public class PuzzleRegionsResult {
    private BufferedImage originalImage;
    private BufferedImage boardSectionImage;
    private BufferedImage piecesSectionImage;
    private int boardSectionInitialCoordinate;
    private int piecesSectionInitialCoordinate;

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }

    public BufferedImage getBoardSectionImage() {
        return boardSectionImage;
    }

    public void setBoardSectionImage(BufferedImage boardSectionImage) {
        this.boardSectionImage = boardSectionImage;
    }

    public BufferedImage getPiecesSectionImage() {
        return piecesSectionImage;
    }

    public void setPiecesSectionImage(BufferedImage piecesSectionImage) {
        this.piecesSectionImage = piecesSectionImage;
    }

    public int getBoardSectionInitialCoordinate() {
        return boardSectionInitialCoordinate;
    }

    public void setBoardSectionInitialCoordinate(int boardSectionInitialCoordinate) {
        this.boardSectionInitialCoordinate = boardSectionInitialCoordinate;
    }

    public int getPiecesSectionInitialCoordinate() {
        return piecesSectionInitialCoordinate;
    }

    public void setPiecesSectionInitialCoordinate(int piecesSectionInitialCoordinate) {
        this.piecesSectionInitialCoordinate = piecesSectionInitialCoordinate;
    }
}
