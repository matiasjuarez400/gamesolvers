package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class PiecesSectionPostProcessingResult {
    private List<PostProcessPiece> postProcessPieces = new ArrayList<>();
    private PiecesSectionResult preResult;

    public List<PostProcessPiece> getPostProcessPieces() {
        return postProcessPieces;
    }

    public PiecesSectionResult getPreResult() {
        return preResult;
    }

    public void setPreResult(PiecesSectionResult preResult) {
        this.preResult = preResult;
    }
}
