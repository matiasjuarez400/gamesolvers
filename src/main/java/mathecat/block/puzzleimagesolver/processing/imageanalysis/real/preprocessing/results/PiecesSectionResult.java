package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results;

import mathecat.block.puzzleimagesolver.model.ImageBlock;

import java.awt.image.BufferedImage;
import java.util.List;

public class PiecesSectionResult {
    private List<ImageBlock> imageBlocks;
    private BufferedImage originalImage;

    public List<ImageBlock> getImageBlocks() {
        return imageBlocks;
    }

    public void setImageBlocks(List<ImageBlock> imageBlocks) {
        this.imageBlocks = imageBlocks;
    }

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }
}
