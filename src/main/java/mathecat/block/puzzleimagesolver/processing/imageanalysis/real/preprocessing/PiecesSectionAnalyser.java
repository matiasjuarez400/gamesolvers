package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.Rectangle;
import mathecat.block.puzzleimagesolver.model.ColorRange;
import mathecat.block.puzzleimagesolver.model.ImageBlock;
import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.PiecesSampleAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PiecesSampleResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PiecesSectionAnalyser extends AbstractAnalyser<PiecesSectionResult> {
    private static final double MAX_PIECE_SIZE_MULTIPLIER = 0.20;
    private static final int PIECE_MIN_SIZE = 5;
    private static final double ACCEPTABLE_DEFORMATION_RANGE = 0.15;

    private PiecesSampleResult piecesSampleResult;
    private int maxPieceSize;

    public PiecesSectionAnalyser() {
        this.piecesSampleResult = new PiecesSampleAnalyser().analyse();
    }

    @Override
    public PiecesSectionResult analyze(BufferedImage bufferedImage) {
        maxPieceSize = (int)(bufferedImage.getWidth() * MAX_PIECE_SIZE_MULTIPLIER);

        PiecesSectionResult result = new PiecesSectionResult();
        result.setOriginalImage(bufferedImage);
        result.setImageBlocks(findBlocks(bufferedImage));

        return result;
    }

    public List<ImageBlock> findBlocks(BufferedImage image) {
        List<ImageBlock> tentativeBlocks = findTentativeImageBlocks(image);
        List<ImageBlock> cleanedBlocks = cleanTentativeImageBlocks(tentativeBlocks);

        return cleanedBlocks;
    }

    private List<ImageBlock> findTentativeImageBlocks(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        List<ImageBlock> tentativeBlocks = new ArrayList<>();

        for (int y = 0; y < height; y++) {
            ROW_FOR: for (int x = 0; x < width; x++) {

                for (ImageBlock imageBlock : tentativeBlocks) {
                    if (imageBlock.getRectangle().containsPoint(x, y)) continue ROW_FOR;
                }

                Pixel currentPixel = new Pixel(image.getRGB(x, y));

                if (isInExpectedColorRanges(currentPixel)) {
                    ImageBlock imageBlock = runBlockSearch(x, y, image);
                    tentativeBlocks.add(imageBlock);

                    x = imageBlock.getRectangle().getLowerRightVertex().x;
                }
            }
        }

        return tentativeBlocks;
    }

    private ImageBlock runBlockSearch(int x, int y, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        int cx;
        int cy;
        for (cx = x + 1; cx < width && (cx - x) < this.maxPieceSize; cx++) {
            Pixel pixel = new Pixel(image.getRGB(cx, y));
            if (!isInExpectedColorRanges(pixel)) {
                break;
            }
        }

        for (cy = y + 1; cy < height && (cy - y) < this.maxPieceSize; cy++) {
            Pixel pixel = new Pixel(image.getRGB(x, cy));
            if (!isInExpectedColorRanges(pixel)) {
                break;
            }
        }

        return new ImageBlock(
                new Rectangle(
                        new Point(x, y),
                        new Point(cx, cy)
                ),
                image.getSubimage(x, y, (cx - x), (cy - y))
        );
    }

    private List<ImageBlock> cleanTentativeImageBlocks(List<ImageBlock> tentativeBlocks) {

        List<ImageBlock> cleanedBlocks = new ArrayList<>(tentativeBlocks);

        Iterator<ImageBlock> iterator = cleanedBlocks.iterator();
        while (iterator.hasNext()) {
            ImageBlock tentativeBlock = iterator.next();

            if (isTooSmall(tentativeBlock) /*|| !hasAcceptableDeformationRate(tentativeBlock)*/) {
                iterator.remove();
            }
        }

        this.cleanOverlapingImageBlocks(cleanedBlocks);

        return cleanedBlocks;
    }

    private void cleanOverlapingImageBlocks(List<ImageBlock> blocks) {
        List<ImageBlock> discardedBlocks = new ArrayList<>();

        for (int i = 0; i < blocks.size(); i++) {
            ImageBlock nextBlock = blocks.get(i);

            if (discardedBlocks.contains(nextBlock)) continue;

            List<ImageBlock> overlapingBlocks = new ArrayList<>();
            overlapingBlocks.add(nextBlock);
            for (ImageBlock possibleOverlapingBlock : blocks) {
                if (possibleOverlapingBlock == nextBlock) continue;

                if (nextBlock.getRectangle().overlapsWith(possibleOverlapingBlock.getRectangle())) {
                    overlapingBlocks.add(possibleOverlapingBlock);
                }
            }

            ImageBlock maxAreaBlock = null;
            for (ImageBlock overlapingBlock : overlapingBlocks) {
                if (maxAreaBlock == null) {
                    maxAreaBlock = overlapingBlock;
                } else {
                    if (overlapingBlock.getRectangle().getArea() > maxAreaBlock.getRectangle().getArea()) {
                        maxAreaBlock = overlapingBlock;
                    }
                }
            }

            overlapingBlocks.remove(maxAreaBlock);

            discardedBlocks.addAll(overlapingBlocks);
        }

        blocks.removeAll(discardedBlocks);
    }

    private boolean isInExpectedColorRanges(Pixel pixel) {
        for (ColorRange colorRange : this.piecesSampleResult.getAllColorRanges()) {
            if (colorRange.isInRange(pixel)) {
                return true;
            }
        }

        return false;
    }

    private boolean isTooSmall(ImageBlock tentativeBlock) {
        return tentativeBlock.getRectangle().getWidth() < PIECE_MIN_SIZE ||
                tentativeBlock.getRectangle().getHeight() < PIECE_MIN_SIZE;
    }

    private boolean hasAcceptableDeformationRate(ImageBlock imageBlock) {
        double deformationRate = imageBlock.getRectangle().getHeight() / imageBlock.getRectangle().getWidth();

        return NumericUtils.isNumberAroundValue(deformationRate, 1, ACCEPTABLE_DEFORMATION_RANGE);
    }
}
