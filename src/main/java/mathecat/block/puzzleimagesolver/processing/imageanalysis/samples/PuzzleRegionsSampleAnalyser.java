package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.puzzleimagesolver.model.Pair;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.PositionedPixel;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PuzzleRegionsSampleResult;

import java.awt.image.BufferedImage;
import java.util.List;

public class PuzzleRegionsSampleAnalyser extends AbstractSampleAnalyser<PuzzleRegionsSampleResult> {
    private static final int DEFAULT_DISTANCE_AROUND_LIMIT = 1;

    private BufferedImage puzzleSampleImage;
    private int distanceAroundLimits;

    public PuzzleRegionsSampleAnalyser(int distanceAroundLimits) {
        this.distanceAroundLimits = distanceAroundLimits;
    }

    public PuzzleRegionsSampleAnalyser() {
        this.distanceAroundLimits = DEFAULT_DISTANCE_AROUND_LIMIT;
    }

    @Override
    public PuzzleRegionsSampleResult analyse() {
        PuzzleRegionsSampleResult result = new PuzzleRegionsSampleResult();
        result.setPixelsAroundUpperRegionLimit(extractSamplesAroundBoardUpperLimitRegion(distanceAroundLimits));
        result.setPixelsAroundLowerRegionLimit(extractSamplesAroundBoardLowerLimitRegion(distanceAroundLimits));

        result.setBrightnessVariationUpperLimit(basicImageAnalyser.calculateZoneBrightnessVariationRange(
                result.getPixelsAroundUpperRegionLimit()
        ));

        result.setBrightnessVariationLowerLimit(basicImageAnalyser.calculateZoneBrightnessVariationRange(
                result.getPixelsAroundLowerRegionLimit()
        ));

        return result;
    }

    private List<Pair<PositionedPixel, PositionedPixel>> extractSamplesAroundBoardUpperLimitRegion(int distanceAroundLimit) {
        return basicImageAnalyser.extractPixelsAroundLine(
                getPuzzleSampleImage(),
                getHorizontalLine(getPuzzleSampleImage(), SampleConstants.PUZZLE_SAMPLE_BOARD_UPPER_LIMIT_POSITION),
                distanceAroundLimit
        );
    }

    private List<Pair<PositionedPixel, PositionedPixel>> extractSamplesAroundBoardLowerLimitRegion(int distanceAroundLimit) {
        return basicImageAnalyser.extractPixelsAroundLine(
                getPuzzleSampleImage(),
                getHorizontalLine(getPuzzleSampleImage(), SampleConstants.PUZZLE_SAMPLE_BOARD_LOWER_LIMIT_POSITION),
                distanceAroundLimit
        );
    }

    private Line getHorizontalLine(BufferedImage image, int yCoordinate) {
        return new Line(
                new Point(0, yCoordinate),
                new Point(image.getWidth(), yCoordinate)
        );
    }

    private BufferedImage getPuzzleSampleImage() {
        if (puzzleSampleImage == null) {
            puzzleSampleImage = imageIOHandler.loadImageByResourceName(SampleConstants.PUZZLE_SAMPLE_SOURCE);
        }

        return puzzleSampleImage;
    }
}
