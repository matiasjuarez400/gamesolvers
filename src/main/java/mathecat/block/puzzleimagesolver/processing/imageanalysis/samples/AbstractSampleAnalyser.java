package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.BasicImageAnalyser;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;

public abstract class AbstractSampleAnalyser<T> {
    protected BasicImageAnalyser basicImageAnalyser;
    protected ImageIOHandler imageIOHandler;

    public AbstractSampleAnalyser() {
        this.basicImageAnalyser = new BasicImageAnalyser();
        this.imageIOHandler = new ImageIOHandler();
    }

    public abstract T analyse();
}
