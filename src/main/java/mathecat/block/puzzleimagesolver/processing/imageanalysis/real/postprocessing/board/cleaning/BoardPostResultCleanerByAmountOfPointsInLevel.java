package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.cleaning;

import mathecat.block.puzzleimagesolver.processing.ProcessingParameters;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.resolutionlogic.model.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BoardPostResultCleanerByAmountOfPointsInLevel implements BoardPostResultCleaner {
    @Override
    public void clean(BoardSectionPostProcessingResult postProcessingResult) {
        int totalPoints = calculateTotalPoints(postProcessingResult);

        cleanPoints(totalPoints, postProcessingResult.getUpPoints());
        cleanPoints(totalPoints, postProcessingResult.getDownPoints());
        cleanPoints(totalPoints, postProcessingResult.getRightPoints());
        cleanPoints(totalPoints, postProcessingResult.getLeftPoints());
    }

    private void cleanPoints(int totalPoints, Map<Integer, List<Point>> pointsMap) {
        List<Integer> levelsToRemove = new ArrayList<>();

        ProcessingParameters processingParameters = ProcessingParameters.getInstance();

        for (Map.Entry<Integer, List<Point>> entry : pointsMap.entrySet()) {

            float pointAmtPercentage = (entry.getValue().size() * 100.0f) / totalPoints;

            if (pointAmtPercentage < processingParameters.BoardPostResultCleanerByAmountOfPointsInLevel_MinimumPercentageTolerance) {
                levelsToRemove.add(entry.getKey());
            }
        }

        for (Integer levelToRemove : levelsToRemove) {
            pointsMap.remove(levelToRemove);
        }
    }

    private int calculateTotalPoints(BoardSectionPostProcessingResult postProcessingResult) {
        int total = 0;

        total += countPointsBySection(postProcessingResult.getUpPoints());
        total += countPointsBySection(postProcessingResult.getDownPoints());
        total += countPointsBySection(postProcessingResult.getRightPoints());
        total += countPointsBySection(postProcessingResult.getLeftPoints());

        return total;
    }

    private int countPointsBySection(Map<Integer, List<Point>> sectionMap) {
        int count = 0;

        for (List<Point> pointList : sectionMap.values()) {
            count += pointList.size();
        }

        return count;
    }
}
