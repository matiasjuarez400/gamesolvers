package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board;

public enum SquareSection {
    UP, RIGHT, DOWN, LEFT
}
