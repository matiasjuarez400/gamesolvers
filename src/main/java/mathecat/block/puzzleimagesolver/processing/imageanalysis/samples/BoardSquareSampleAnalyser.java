package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.BoardSquareSampleResult;

import java.awt.image.BufferedImage;

public class BoardSquareSampleAnalyser extends AbstractSampleAnalyser<BoardSquareSampleResult> {
    private static final int DEFAULT_DISTANCE = 2;

    private int distanceAroundLimit;
    private BufferedImage boardSquareSampleImage;

    public BoardSquareSampleAnalyser(int distanceAroundLimit) {
        this.distanceAroundLimit = distanceAroundLimit;
    }

    public BoardSquareSampleAnalyser() {
        this(DEFAULT_DISTANCE);
    }

    @Override
    public BoardSquareSampleResult analyse() {
        BoardSquareSampleResult result = new BoardSquareSampleResult();

        result.setUpperLimit(calculateBoardSquareRangeAroundLine(SampleConstants.BOARD_SQUARE_UPPER_LIMIT, distanceAroundLimit));
        result.setRightLimit(calculateBoardSquareRangeAroundLine(SampleConstants.BOARD_SQUARE_RIGHT_LIMIT, distanceAroundLimit));
        result.setBottomLimit(calculateBoardSquareRangeAroundLine(SampleConstants.BOARD_SQUARE_BOTTOM_LIMIT, distanceAroundLimit));
        result.setLeftLimit(calculateBoardSquareRangeAroundLine(SampleConstants.BOARD_SQUARE_LEFT_LIMIT, distanceAroundLimit));

        return result;
    }

    private Range calculateBoardSquareRangeAroundLine(Line line, int distanceAroundLimit) {
        return basicImageAnalyser.calculateBrightnessVariationRangeAroundLine(
                getBoardSquareSampleImage(), line, distanceAroundLimit).expandToleranceByProportion(1);
    }

    private BufferedImage getBoardSquareSampleImage() {
        if (boardSquareSampleImage == null) {
            boardSquareSampleImage = imageIOHandler.loadImageByResourceName(SampleConstants.BOARD_SQUARE);
        }

        return boardSquareSampleImage;
    }
}
