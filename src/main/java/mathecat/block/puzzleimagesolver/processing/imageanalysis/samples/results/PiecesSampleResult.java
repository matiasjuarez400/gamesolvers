package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results;

import mathecat.block.puzzleimagesolver.model.ColorRange;

import java.util.Arrays;
import java.util.List;

public class PiecesSampleResult {
    private ColorRange greenColorRange;
    private ColorRange lightBlueColorRange;
    private ColorRange pinkColorRange;
    private ColorRange purpleColorRange;
    private ColorRange redColorRange;
    private ColorRange yellowColorRange;

    public ColorRange getGreenColorRange() {
        return greenColorRange;
    }

    public void setGreenColorRange(ColorRange greenColorRange) {
        this.greenColorRange = greenColorRange;
    }

    public ColorRange getLightBlueColorRange() {
        return lightBlueColorRange;
    }

    public void setLightBlueColorRange(ColorRange lightBlueColorRange) {
        this.lightBlueColorRange = lightBlueColorRange;
    }

    public ColorRange getPinkColorRange() {
        return pinkColorRange;
    }

    public void setPinkColorRange(ColorRange pinkColorRange) {
        this.pinkColorRange = pinkColorRange;
    }

    public ColorRange getPurpleColorRange() {
        return purpleColorRange;
    }

    public void setPurpleColorRange(ColorRange purpleColorRange) {
        this.purpleColorRange = purpleColorRange;
    }

    public ColorRange getRedColorRange() {
        return redColorRange;
    }

    public void setRedColorRange(ColorRange redColorRange) {
        this.redColorRange = redColorRange;
    }

    public ColorRange getYellowColorRange() {
        return yellowColorRange;
    }

    public void setYellowColorRange(ColorRange yellowColorRange) {
        this.yellowColorRange = yellowColorRange;
    }

    public List<ColorRange> getAllColorRanges() {
        return Arrays.asList(
            getGreenColorRange(),
            getLightBlueColorRange(),
            getPinkColorRange(),
            getPurpleColorRange(),
            getRedColorRange(),
            getYellowColorRange()
        );
    }
}
