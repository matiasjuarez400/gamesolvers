package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing;

import mathecat.block.puzzleimagesolver.model.Rectangle;
import mathecat.block.puzzleimagesolver.model.ImageBlock;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PiecesSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PostProcessPiece;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

import java.util.*;

public class PiecesSectionResultPostProcessor {
    private static final double AVG_BLOCK_SIZE_INNER_SPACE_MULTIPLIER = 0.16;

    public PiecesSectionPostProcessingResult analyse(PiecesSectionResult preResult) {
        PiecesSectionPostProcessingResult postResult = new PiecesSectionPostProcessingResult();
        postResult.setPreResult(preResult);

        postResult.getPostProcessPieces().addAll(joinNearPieces(preResult));

        for (PostProcessPiece postProcessPiece : postResult.getPostProcessPieces()) {
            postProcessPiece.setModel(generatePieceModel(postProcessPiece));
        }

        return postResult;
    }

    private boolean[][] generatePieceModel(PostProcessPiece piece) {
        Rectangle avgRectangle = calculateAverageRectangle(piece.getBlocks());

        double verticalFusionWindow = avgRectangle.getHeight() / 2.0;
        double horizontalFusionWindow = avgRectangle.getWidth() / 2.0;

        Map<Integer, List<ImageBlock>> verticalLevels = new HashMap<>();
        Map<Integer, List<ImageBlock>> horizontalLevels = new HashMap<>();

        for (ImageBlock imageBlock : piece.getBlocks()) {
            boolean fitVert = false;
            boolean fitHor = false;

            for (Map.Entry<Integer, List<ImageBlock>> entry : verticalLevels.entrySet()) {
                if (NumericUtils.isNumberAroundValue(imageBlock.getRectangle().getUpperLeftVertex().y, entry.getKey(), verticalFusionWindow)) {
                    entry.getValue().add(imageBlock);
                    fitVert = true;
                    break;
                }
            }

            for (Map.Entry<Integer, List<ImageBlock>> entry : horizontalLevels.entrySet()) {
                if (NumericUtils.isNumberAroundValue(imageBlock.getRectangle().getUpperLeftVertex().x, entry.getKey(), horizontalFusionWindow)) {
                    entry.getValue().add(imageBlock);
                    fitHor = true;
                    break;
                }
            }

            if (!fitVert) {
                List<ImageBlock> imageBlocks = new ArrayList<>();
                imageBlocks.add(imageBlock);
                verticalLevels.put(imageBlock.getRectangle().getUpperLeftVertex().y, imageBlocks);
            }

            if (!fitHor) {
                List<ImageBlock> imageBlocks = new ArrayList<>();
                imageBlocks.add(imageBlock);
                horizontalLevels.put(imageBlock.getRectangle().getUpperLeftVertex().x, imageBlocks);
            }
        }

        return doGeneratePiecemodel(verticalLevels, horizontalLevels);
    }

    private boolean[][] doGeneratePiecemodel(Map<Integer, List<ImageBlock>> verticalLevels, Map<Integer, List<ImageBlock>> horizontalLevels) {
        boolean[][] pieceModel = new boolean[verticalLevels.size()][horizontalLevels.size()];
        List<Map.Entry<Integer, List<ImageBlock>>> verticalLevelsList = new ArrayList<>(verticalLevels.entrySet());
        List<Map.Entry<Integer, List<ImageBlock>>> horizontalLevelsList = new ArrayList<>(horizontalLevels.entrySet());

        Comparator<Map.Entry<Integer, List<ImageBlock>>> levelComparator = (e1, e2) -> e1.getKey() - e2.getKey();

        Collections.sort(verticalLevelsList, levelComparator);
        Collections.sort(horizontalLevelsList, levelComparator);

        for (int y = 0; y < verticalLevelsList.size(); y++) {
            for (ImageBlock verticalBlock : verticalLevelsList.get(y).getValue()) {
                boolean foundMatchingHorizontalBlock = false;
                HOR_IT: for (int x = 0; x < horizontalLevelsList.size(); x++) {
                    for (ImageBlock horizontalBlock : horizontalLevelsList.get(x).getValue()) {
                        if (verticalBlock.equals(horizontalBlock)) {
                            pieceModel[y][x] = true;
                            foundMatchingHorizontalBlock = true;
                            break HOR_IT;
                        }
                    }
                }

                if (!foundMatchingHorizontalBlock) {
                    throw new IllegalStateException("Unable to position imageBlock in pieceModel");
                }
            }
        }

        return pieceModel;
    }

    private List<PostProcessPiece> joinNearPieces(PiecesSectionResult preResult) {
        Rectangle avgRectangle = calculateAverageRectangle(preResult);
        double verticalTolerance = avgRectangle.getHeight() * AVG_BLOCK_SIZE_INNER_SPACE_MULTIPLIER;
        double horizontalTolerance = avgRectangle.getWidth() * AVG_BLOCK_SIZE_INNER_SPACE_MULTIPLIER;
        double distanceTolerance = Math.sqrt(Math.pow(verticalTolerance, 2) + Math.pow(horizontalTolerance, 2));

        List<PostProcessPiece> postProcessPieces = new ArrayList<>();

        Set<ImageBlock> usedBlocks = new HashSet<>();

        for (ImageBlock imageBlock : preResult.getImageBlocks()) {
            if (usedBlocks.contains(imageBlock)) continue;

            usedBlocks.add(imageBlock);

            PostProcessPiece postProcessPiece = new PostProcessPiece();
            postProcessPieces.add(postProcessPiece);
            postProcessPiece.addBlock(imageBlock);

            boolean foundNextBlock = true;
            while (foundNextBlock) {
                boolean couldJoinUnusedBlock = false;
                UNUSED_PIECES_FOR: for (ImageBlock unusedImageBlock : preResult.getImageBlocks()) {
                    if (usedBlocks.contains(unusedImageBlock)) continue;

                    for (ImageBlock pieceBlock : postProcessPiece.getBlocks()) {
                        if (pieceBlock.getRectangle().calculateClosestVertexDistance(unusedImageBlock.getRectangle()) <= distanceTolerance) {
                            usedBlocks.add(unusedImageBlock);
                            postProcessPiece.addBlock(unusedImageBlock);
                            couldJoinUnusedBlock = true;
                            break UNUSED_PIECES_FOR;
                        }
                    }
                }

                if (!couldJoinUnusedBlock) {
                    foundNextBlock = false;
                }
            }
        }

        return postProcessPieces;
    }

    private Rectangle calculateAverageRectangle(PiecesSectionResult preResult) {
        return calculateAverageRectangle(preResult.getImageBlocks());
    }

    private Rectangle calculateAverageRectangle(List<ImageBlock> imageBlocks) {
        int avgWidth = 0;
        int avgHeight = 0;
        int blockAmt = imageBlocks.size();

        for (ImageBlock imageBlock : imageBlocks) {
            avgWidth += imageBlock.getRectangle().getWidth();
            avgHeight += imageBlock.getRectangle().getHeight();
        }

        avgWidth /= blockAmt;
        avgHeight /= blockAmt;

        return new Rectangle(0, 0, avgWidth, avgHeight);
    }
}
