package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.AnomalyDataKit;
import mathecat.block.resolutionlogic.model.Point;

import java.util.*;

public class BoardResultCleanerByLevelDistance {
    private static final double ACCEPTABLE_PROPORTION_DEVIATION = 0.10;

    public void clean(BoardSectionPostProcessingResult postResult) {
        Map<SquareSection, List<Map.Entry<Integer, List<Point>>>> pointsSortedByLevel =
                sortlevelsByLevelPosition(postResult.getAllPointsMessyCode());

        for (Map.Entry<SquareSection, List<Map.Entry<Integer, List<Point>>>> entry : pointsSortedByLevel.entrySet()) {
            removeAnomalousLevels(entry.getValue());
        }
    }

    private void removeAnomalousLevels(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        removeLevels(pointsByLevel);
        Collections.reverse(pointsByLevel);
        removeLevels(pointsByLevel);
    }

    private void removeLevels(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        List<Double> interLevelDistances = calculateInterLevelDistance(pointsByLevel);
        AnomalyDataKit anomalyDataKit = AnomalyDataKit.create(interLevelDistances);

        List<Map.Entry<Integer, List<Point>>> mostMatchingLevels = new ArrayList<>();

        for (int i = 0; i < pointsByLevel.size() - 1; i++) {
            Map.Entry<Integer, List<Point>> currentStartingLevel = pointsByLevel.get(i);

            List<Map.Entry<Integer, List<Point>>> acceptableLevels = new ArrayList<>();
            acceptableLevels.add(currentStartingLevel);

            double middlePosition = currentStartingLevel.getKey();

            for (int j = i + 1; j < pointsByLevel.size(); j++) {
                middlePosition += anomalyDataKit.getQ2();

                if (NumericUtils.isNumberAroundValue(
                        pointsByLevel.get(j).getKey(),
                        middlePosition,
                        anomalyDataKit.getTrunkedAvg() * ACCEPTABLE_PROPORTION_DEVIATION
                    )
                ) {
                    acceptableLevels.add(pointsByLevel.get(j));
                }
            }

            if (acceptableLevels.size() > mostMatchingLevels.size()) {
                mostMatchingLevels = acceptableLevels;
            }
        }

        Iterator<Map.Entry<Integer, List<Point>>> iterator = pointsByLevel.iterator();

        while (iterator.hasNext()) {
            Map.Entry<Integer, List<Point>> pointsByLevelEntry = iterator.next();

            if (!mostMatchingLevels.contains(pointsByLevelEntry)) {
                iterator.remove();
            }
        }

//        boolean needMoreIterations = true;
//
//        while (needMoreIterations) {
//            needMoreIterations = false;
//
//            for (int i = 0; i < pointsByLevel.size() - 1; i++) {
//                Map.Entry<Integer, List<Point>> currentLevel = pointsByLevel.get(i);
//
//                boolean foundAcceptableDistance = false;
//                for (int j = i + 1; j < pointsByLevel.size(); j++) {
//                    Map.Entry<Integer, List<Point>> nextLevel = pointsByLevel.get(j);
//
//                    double currentDistance = Math.abs(currentLevel.getKey() - nextLevel.getKey());
//                    if (NumericUtils.isNumberAroundValue(
//                            currentDistance,
//                            anomalyDataKit.getTrunkedAvg(),
//                            anomalyDataKit.getTrunkedAvg() * ACCEPTABLE_PROPORTION_DEVIATION
//                        )
//                    ) {
//                        foundAcceptableDistance = true;
//                        break;
//
//                    }
//                }
//
//                if (!foundAcceptableDistance) {
//                    pointsByLevel.remove(currentLevel);
//                    needMoreIterations = true;
//                    interLevelDistances = calculateInterLevelDistance(pointsByLevel);
//                    anomalyDataKit = AnomalyDataKit.create(interLevelDistances);
//                    break;
//                }
//            }
//        }
    }

    private List<Double> calculateInterLevelDistance(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        List<Map.Entry<Integer, List<Point>>> sortedList = sortPointsByLevel(pointsByLevel);

        List<Double> sortedLevels = new ArrayList<>();
        for (Map.Entry<Integer, List<Point>> entry : sortedList) {
            sortedLevels.add(Double.valueOf(entry.getKey()));
        }

        List<Double> distances = new ArrayList<>();
        for (int i = 0; i < sortedLevels.size() - 1; i++) {
            distances.add(Math.abs(sortedLevels.get(i) - sortedLevels.get(i + 1)));
        }

        return distances;
    }

    private List<Map.Entry<Integer, List<Point>>> sortPointsByLevel(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        Comparator<Map.Entry<Integer, List<Point>>> entryComparator = (e1, e2) -> e1.getKey() - e2.getKey();

        List<Map.Entry<Integer, List<Point>>> mapEntry = new ArrayList<>(pointsByLevel);

        Collections.sort(mapEntry, entryComparator);

        return mapEntry;
    }

    private Map<SquareSection, List<Map.Entry<Integer, List<Point>>>>
        sortlevelsByLevelPosition(Map<SquareSection, Map<Integer, List<Point>>> allPointsByLevelMap) {

        Map<SquareSection, List<Map.Entry<Integer, List<Point>>>> allPointsSortedByLevelPosition = new HashMap<>();

        for (Map.Entry<SquareSection, Map<Integer, List<Point>>> entry :
            allPointsByLevelMap.entrySet()) {
            allPointsSortedByLevelPosition.put(entry.getKey(), sortLevelsByLevelPosition(entry.getValue()));
        }

        return allPointsSortedByLevelPosition;
    }

    private List<Map.Entry<Integer, List<Point>>> sortLevelsByLevelPosition(Map<Integer, List<Point>> resultEntry) {
        Comparator<Map.Entry<Integer, List<Point>>> entryComparator = (e1, e2) -> e1.getKey() - e2.getKey();

        List<Map.Entry<Integer, List<Point>>> mapEntry = new ArrayList<>(resultEntry.entrySet());

        Collections.sort(mapEntry, entryComparator);

        return mapEntry;
    }
}
