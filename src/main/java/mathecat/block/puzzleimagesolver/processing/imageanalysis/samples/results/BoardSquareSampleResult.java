package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results;

import mathecat.block.puzzleimagesolver.model.Range;

public class BoardSquareSampleResult {
    private Range upperLimit;
    private Range bottomLimit;
    private Range leftLimit;
    private Range rightLimit;

    public Range getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(Range upperLimit) {
        this.upperLimit = upperLimit;
    }

    public Range getBottomLimit() {
        return bottomLimit;
    }

    public void setBottomLimit(Range bottomLimit) {
        this.bottomLimit = bottomLimit;
    }

    public Range getLeftLimit() {
        return leftLimit;
    }

    public void setLeftLimit(Range leftLimit) {
        this.leftLimit = leftLimit;
    }

    public Range getRightLimit() {
        return rightLimit;
    }

    public void setRightLimit(Range rightLimit) {
        this.rightLimit = rightLimit;
    }
}
