package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results;

import lombok.Getter;
import lombok.Setter;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.SquareSection;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.resolutionlogic.model.Point;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter @Setter
public class BoardSectionPostProcessingResult {
    private boolean[][] model;

    private Map<Integer, List<Point>> upPoints;
    private Map<Integer, List<Point>> downPoints;
    private Map<Integer, List<Point>> rightPoints;
    private Map<Integer, List<Point>> leftPoints;
    private BufferedImage detectedBoard;
    private String detectedBoardBase64;

    private BoardSectionResult preResult;

    public Map<SquareSection, Map<Integer, List<Point>>> getAllPointsMessyCode() {
        Map<SquareSection, Map<Integer, List<Point>>> allPointsMap = new HashMap<>();

        allPointsMap.put(SquareSection.UP, upPoints);
        allPointsMap.put(SquareSection.DOWN, downPoints);
        allPointsMap.put(SquareSection.RIGHT, rightPoints);
        allPointsMap.put(SquareSection.LEFT, leftPoints);

        return allPointsMap;
    }

    public BoardSectionResult getPreResult() {
        return preResult;
    }

    public void setPreResult(BoardSectionResult preResult) {
        this.preResult = preResult;
    }

    public List<Point> getAllPoints() {
        List<Point> pointList = new ArrayList<>();

        addPointsFromMap(pointList, upPoints);
        addPointsFromMap(pointList, downPoints);
        addPointsFromMap(pointList, rightPoints);
        addPointsFromMap(pointList, leftPoints);

        return pointList;
    }

    private void addPointsFromMap(List<Point> pointList, Map<Integer, List<Point>> mapPoint) {
        if (mapPoint == null) return;

        for (List<Point> listInMap : mapPoint.values()) {
            pointList.addAll(listInMap);
        }
    }
}
