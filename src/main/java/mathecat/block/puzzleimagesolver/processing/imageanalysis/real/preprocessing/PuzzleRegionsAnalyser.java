package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.BasicImageAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PuzzleRegionsResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.PuzzleRegionsSampleAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PuzzleRegionsSampleResult;
import mathecat.block.puzzleimagesolver.processing.utils.DataCleaner;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class PuzzleRegionsAnalyser extends AbstractAnalyser<PuzzleRegionsResult> {
    private PuzzleRegionsSampleResult puzzleRegionsSampleResult;

    private static final int SECTIONS_TO_ANALYZE = 40;
    private static final int PIXELS_IN_SECTION_TO_ANALYZE = 1;

    private static final int PIXELS_TO_ANALYZE_TOGETHER = 3;

    private static final int VERTICAL_DISTANCE_BETWEEN_PIXELS_TOLERANCE = 2;
    private static final int DISTANCE_AROUND_LIMIT_TO_USE = 3;

    public PuzzleRegionsAnalyser() {
        this.puzzleRegionsSampleResult = new PuzzleRegionsSampleAnalyser(DISTANCE_AROUND_LIMIT_TO_USE).analyse();
    }

    @Override
    public PuzzleRegionsResult analyze(BufferedImage bufferedImage) {
        int initialYSearchForPieces = (int) (bufferedImage.getHeight() * 0.3);
        int boardZoneUpperLimit = detectInitialYForZone(
                bufferedImage,
                puzzleRegionsSampleResult.getBrightnessVariationUpperLimit().lowerLimit,
                puzzleRegionsSampleResult.getBrightnessVariationUpperLimit().upperLimit,
                0, initialYSearchForPieces);

        int piecesZoneUpperLimit = detectInitialYForZone(
                bufferedImage,
                puzzleRegionsSampleResult.getBrightnessVariationLowerLimit().lowerLimit,
                puzzleRegionsSampleResult.getBrightnessVariationLowerLimit().upperLimit,
                initialYSearchForPieces, bufferedImage.getHeight() - initialYSearchForPieces);

        PuzzleRegionsResult result = new PuzzleRegionsResult();
        result.setOriginalImage(bufferedImage);

        result.setBoardSectionImage(bufferedImage.getSubimage(0, boardZoneUpperLimit,
                bufferedImage.getWidth(), piecesZoneUpperLimit - boardZoneUpperLimit));

        result.setPiecesSectionImage(bufferedImage.getSubimage(
                0, piecesZoneUpperLimit, bufferedImage.getWidth(), bufferedImage.getHeight() - piecesZoneUpperLimit
        ));

        result.setBoardSectionInitialCoordinate(boardZoneUpperLimit);
        result.setPiecesSectionInitialCoordinate(piecesZoneUpperLimit);

        return result;
    }

    private int detectInitialYForZone(BufferedImage bufferedImage, double minVariation, double maxVariation,
                                      int initialYSearch, int endYSearch) {
        bufferedImage = bufferedImage.getSubimage(0, initialYSearch, bufferedImage.getWidth(), endYSearch);

        int verticalSectionSize = bufferedImage.getWidth() / SECTIONS_TO_ANALYZE;

        List<Point> pointsOnDivisionLine = new ArrayList<>();

        for (int i = 0; i < SECTIONS_TO_ANALYZE; i++) {
            int x = i * verticalSectionSize + verticalSectionSize / 2;

            List<Point> pointsAroundBrightnessVariation = super.basicImageAnalyser.findZonesWithBrightnessVariationInRange(
                    bufferedImage.getSubimage(x, 0, PIXELS_IN_SECTION_TO_ANALYZE, bufferedImage.getHeight()),
                    minVariation, maxVariation,
                    PIXELS_TO_ANALYZE_TOGETHER, BasicImageAnalyser.SearchDirection.VERTICAL
            );

            pointsOnDivisionLine.addAll(pointsAroundBrightnessVariation);
        }

        List<Point> cleanedPoints =
                DataCleaner.keepPointsConcentratedVertically(pointsOnDivisionLine, VERTICAL_DISTANCE_BETWEEN_PIXELS_TOLERANCE);

        Point centerPoint = NumericUtils.calculateRangeCenter(cleanedPoints);

        return centerPoint.y + initialYSearch;
    }
}
