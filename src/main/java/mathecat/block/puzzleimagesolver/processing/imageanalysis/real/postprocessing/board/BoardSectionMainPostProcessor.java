package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board;

import mathecat.block.puzzleimagesolver.processing.ImageBase64Converter;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.cleaning.BoardPostResultCleaner;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.cleaning.BoardPostResultCleanerByAmountOfPointsInLevel;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.StatisticalUtils;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.resolutionlogic.model.Point;

import java.awt.image.BufferedImage;
import java.util.*;

public class BoardSectionMainPostProcessor {
    private static final double PROPORTIONAL_DISTANCE_FROM_AVG_PERMITED = 0.80;
    private static final double AVG_EXTREME_SIZES_TOLERANCE = 0.90;
    private BoardLevelFusionPostProcessor boardLevelFusionPostProcessor;
    private BoardResultCleanerByLevelDistance boardResultCleanerByLevelDistance;
    private BoardPostResultCleaner pointsPercentageCleaner;
    private ImageModifier imageModifier;
    private ImageBase64Converter imageBase64Converter;

    public BoardSectionMainPostProcessor() {
        this.boardLevelFusionPostProcessor = new BoardLevelFusionPostProcessor();
        this.boardResultCleanerByLevelDistance = new BoardResultCleanerByLevelDistance();
        this.pointsPercentageCleaner = new BoardPostResultCleanerByAmountOfPointsInLevel();
        this.imageModifier = new ImageModifier();
        this.imageBase64Converter = new ImageBase64Converter();
    }

    public BoardSectionPostProcessingResult analyse(BoardSectionResult preResult) {
        BoardSectionPostProcessingResult postResult = new BoardSectionPostProcessingResult();
        postResult.setPreResult(preResult);

        this.boardLevelFusionPostProcessor.analyse(preResult, postResult);
//        this.boardResultCleanerByLevelDistance.clean(postResult);
//
//        cleanPostResult(postResult);
        this.pointsPercentageCleaner.clean(postResult);

        postResult.setDetectedBoard(getDetectedBoard(postResult));
        postResult.setDetectedBoardBase64(imageBase64Converter.convert(postResult.getDetectedBoard()));

        postResult.setModel(buildRepresentationMatrix(postResult.getRightPoints(), postResult.getDownPoints()));

        return postResult;
    }

    private BufferedImage getDetectedBoard(BoardSectionPostProcessingResult postResult) {
        BufferedImage boardDetectedImage =
                imageModifier.copyImage(postResult.getPreResult().getOriginalImage());

        imageModifier.drawPointsInImage(boardDetectedImage, postResult.getUpPoints(), false);
        imageModifier.drawPointsInImage(boardDetectedImage, postResult.getDownPoints(), false);
        imageModifier.drawPointsInImage(boardDetectedImage, postResult.getLeftPoints(), false);
        imageModifier.drawPointsInImage(boardDetectedImage, postResult.getRightPoints(), false);

        return boardDetectedImage;
    }

    private void removeLevelsWithSizeTooFarFromAvg(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        List<Double> sizes = new ArrayList<>();
        for (Map.Entry<Integer, List<Point>> entry : pointsByLevel) {
            sizes.add((double) entry.getValue().size());
        }

        double avg = StatisticalUtils.average(sizes);
        double minValue = avg * (1 - PROPORTIONAL_DISTANCE_FROM_AVG_PERMITED);
        double maxValue = avg * (1 + PROPORTIONAL_DISTANCE_FROM_AVG_PERMITED * 30);

        Iterator<Map.Entry<Integer, List<Point>>> iterator = pointsByLevel.iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, List<Point>> next = iterator.next();

            if (!NumericUtils.isNumberInRange(next.getValue().size(),
                    minValue,
                    maxValue
            )) {
                iterator.remove();
            }
        }
    }

    private List<Map.Entry<Integer, List<Point>>> removeExtremeSizes(final List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        List<Map.Entry<Integer, List<Point>>> sortedList = sortPointsByLevel(pointsByLevel);

        boolean removedValue;

        do {
            double sizeAvg = 0;
            for (Map.Entry<Integer, List<Point>> entry : sortedList) {
                sizeAvg += entry.getValue().size();
            }
            sizeAvg /= sortedList.size();

            double minValue = sizeAvg * (1 - AVG_EXTREME_SIZES_TOLERANCE);
            double maxValue = sizeAvg * (1 + AVG_EXTREME_SIZES_TOLERANCE);

            Iterator<Map.Entry<Integer, List<Point>>> iterator = sortedList.iterator();
            removedValue = false;
            while (iterator.hasNext()) {
                Map.Entry<Integer, List<Point>> next = iterator.next();

                int nextValue = next.getKey();

                if (!NumericUtils.isNumberInRange(
                        nextValue,
                        minValue,
                        maxValue
                )) {
                    iterator.remove();
                    removedValue = true;
                }
            }
        } while (removedValue);

        return sortedList;
    }

    //returns a matrix that contains a boolean that indicates if a square exists or not in each matrix position
    private boolean[][] buildRepresentationMatrix(Map<Integer, List<Point>> rightPointsMap, Map<Integer, List<Point>> downPointsMap) {
        List<Map.Entry<Integer, List<Point>>> rightPoints = sortPostProcessingResultEntriesByLevel(rightPointsMap);
        List<Map.Entry<Integer, List<Point>>> downPoints = sortPostProcessingResultEntriesByLevel(downPointsMap);

        boolean[][] matrix = new boolean[downPointsMap.size()][rightPoints.size()];

        for (int x = 0; x < rightPoints.size(); x++) {
            Map.Entry<Integer, List<Point>> rightPointsEntry = rightPoints.get(x);

            for (int y = 0; y < downPoints.size(); y++) {
                int previousLevel = y == 0 ? 0 : downPoints.get(y - 1).getKey();
                int currentLevel = downPoints.get(y).getKey();

                for (Point point : rightPointsEntry.getValue()) {
                    if (NumericUtils.isNumberInRange(point.y, previousLevel, currentLevel)) {
                        matrix[y][x] = true;
                        break;
                    }
                }
            }
        }

        return matrix;
    }

    private void putEntriesIntoMap(List<Map.Entry<Integer, List<Point>>> entries, Map<Integer, List<Point>> map) {
        map.clear();
        for (Map.Entry<Integer, List<Point>> entry : entries) {
            map.put(entry.getKey(), entry.getValue());
        }
    }

    private void trimResultEntries(List<Map.Entry<Integer, List<Point>>> e1, List<Map.Entry<Integer, List<Point>>> e2) {
        int minSize = e1.size() < e2.size() ? e1.size() : e2.size();

        while (e1.size() > minSize) {
            e1.remove(e1.size() - 1);
        }

        while (e2.size() > minSize) {
            e2.remove(e2.size() - 1);
        }
    }

    private List<Map.Entry<Integer, List<Point>>> sortPostProcessingResultEntriesByLevel(Map<Integer, List<Point>> resultEntry) {
        return sortPointsByLevel(new ArrayList<>(resultEntry.entrySet()));
    }

    private List<Map.Entry<Integer, List<Point>>> sortPointsByLevel(List<Map.Entry<Integer, List<Point>>> pointsByLevel) {
        Comparator<Map.Entry<Integer, List<Point>>> entryComparator = (e1, e2) -> e1.getKey() - e2.getKey();

        List<Map.Entry<Integer, List<Point>>> mapEntry = new ArrayList<>(pointsByLevel);

        Collections.sort(mapEntry, entryComparator);

        return mapEntry;
    }

    private List<Map.Entry<Integer, List<Point>>> sortPostProcessingResultEntriesBySize(Map<Integer, List<Point>> resultEntry) {
        Comparator<Map.Entry<Integer, List<Point>>> entryComparator = (e1, e2) -> e2.getValue().size() - e1.getValue().size();

        List<Map.Entry<Integer, List<Point>>> mapEntry = new ArrayList<>(resultEntry.entrySet());

        Collections.sort(mapEntry, entryComparator);

        return mapEntry;
    }
}
