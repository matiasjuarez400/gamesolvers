package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results;

import mathecat.block.puzzleimagesolver.model.Pair;
import mathecat.block.puzzleimagesolver.model.PositionedPixel;
import mathecat.block.puzzleimagesolver.model.Range;

import java.util.List;

public class PuzzleRegionsSampleResult {
    private List<Pair<PositionedPixel, PositionedPixel>> pixelsAroundUpperRegionLimit;
    private Range brightnessVariationUpperLimit;

    private List<Pair<PositionedPixel, PositionedPixel>> pixelsAroundLowerRegionLimit;
    private Range brightnessVariationLowerLimit;

    public List<Pair<PositionedPixel, PositionedPixel>> getPixelsAroundUpperRegionLimit() {
        return pixelsAroundUpperRegionLimit;
    }

    public void setPixelsAroundUpperRegionLimit(List<Pair<PositionedPixel, PositionedPixel>> pixelsAroundUpperRegionLimit) {
        this.pixelsAroundUpperRegionLimit = pixelsAroundUpperRegionLimit;
    }

    public Range getBrightnessVariationUpperLimit() {
        return brightnessVariationUpperLimit;
    }

    public void setBrightnessVariationUpperLimit(Range brightnessVariationUpperLimit) {
        this.brightnessVariationUpperLimit = brightnessVariationUpperLimit;
    }

    public List<Pair<PositionedPixel, PositionedPixel>> getPixelsAroundLowerRegionLimit() {
        return pixelsAroundLowerRegionLimit;
    }

    public void setPixelsAroundLowerRegionLimit(List<Pair<PositionedPixel, PositionedPixel>> pixelsAroundLowerRegionLimit) {
        this.pixelsAroundLowerRegionLimit = pixelsAroundLowerRegionLimit;
    }

    public Range getBrightnessVariationLowerLimit() {
        return brightnessVariationLowerLimit;
    }

    public void setBrightnessVariationLowerLimit(Range brightnessVariationLowerLimit) {
        this.brightnessVariationLowerLimit = brightnessVariationLowerLimit;
    }
}
