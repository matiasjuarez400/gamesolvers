package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.cleaning;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;

public interface BoardPostResultCleaner {
    void clean(BoardSectionPostProcessingResult postProcessingResult);
}
