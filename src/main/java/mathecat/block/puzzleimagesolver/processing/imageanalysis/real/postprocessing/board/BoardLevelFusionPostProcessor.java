package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.resolutionlogic.model.Point;

import java.util.*;

public class BoardLevelFusionPostProcessor {
    private static final int DEFAULT_MAX_DISTANCE_TO_FUSIONATE = 3;
    private static final int DEFAULT_FUSION_ITERATIONS = 3;

    private final int maxDistanceToFusionate;
    private final int fusionIterations;

    public BoardLevelFusionPostProcessor(int maxDistanceToFusionate, int fusionIterations) {
        this.maxDistanceToFusionate = maxDistanceToFusionate;
        this.fusionIterations = fusionIterations;
    }

    public BoardLevelFusionPostProcessor() {
        this(DEFAULT_MAX_DISTANCE_TO_FUSIONATE, DEFAULT_FUSION_ITERATIONS);
    }

    public BoardSectionPostProcessingResult analyse(BoardSectionResult preResult, BoardSectionPostProcessingResult postResult) {
        postResult.setUpPoints(processPoints(preResult.getUpPoints(), SquareSection.UP));
        postResult.setDownPoints(processPoints(preResult.getDownPoints(), SquareSection.DOWN));
        postResult.setLeftPoints(processPoints(preResult.getLeftPoints(), SquareSection.LEFT));
        postResult.setRightPoints(processPoints(preResult.getRightPoints(), SquareSection.RIGHT));

        return postResult;
    }

    private Map<Integer, List<Point>> processPoints(List<Point> points, SquareSection squareSection){
        Map<Integer, List<Point>> processedPoints = alignPoints(points, squareSection);

        for (int i = 0; i < fusionIterations; i++) {
            processedPoints = fusionLevels(processedPoints);
        }

        return processedPoints;
    }

    private Map<Integer, List<Point>> fusionLevels(Map<Integer, List<Point>> pointsByLevel) {
        List<Map.Entry<Integer, List<Point>>> mapEntriesByListSize = new ArrayList<>(pointsByLevel.entrySet());
        Collections.sort(mapEntriesByListSize, (es1, es2) -> es2.getValue().size() - es1.getValue().size());

        Map<Integer, List<Point>> fusionedMap = new HashMap<>();
        Set<Integer> processedLevel = new HashSet<>();
        for (Map.Entry<Integer, List<Point>> mapEntryByListSize : mapEntriesByListSize) {
            if (processedLevel.contains(mapEntryByListSize.getKey())) continue;

            List<Point> fusionedPoints = new ArrayList<>();

            int currentFusioningLevel = mapEntryByListSize.getKey();
            int currentFusionLevelSize = pointsByLevel.get(currentFusioningLevel).size();

            int lowLimit = currentFusioningLevel - maxDistanceToFusionate;
            int upperLimit = currentFusioningLevel + maxDistanceToFusionate;
            for (int surroundingLevel = lowLimit; surroundingLevel < upperLimit; surroundingLevel++) {
                processedLevel.add(surroundingLevel);

                List<Point> pointsAtLeveli = pointsByLevel.get(surroundingLevel);

                if (pointsAtLeveli != null) {
                    fusionedPoints.addAll(pointsAtLeveli);

                    if (pointsAtLeveli.size() > currentFusionLevelSize) {
                        currentFusioningLevel = surroundingLevel;
                        currentFusionLevelSize = pointsByLevel.get(currentFusioningLevel).size();
                    }
                }
            }

            fusionedMap.put(currentFusioningLevel, fusionedPoints);
        }

        return fusionedMap;
    }

    private Map<Integer, List<Point>> alignPoints(List<Point> points, SquareSection squareSection) {
        Map<Integer, List<Point>> alignedPointsMap = new HashMap<>();

        for (Point point : points) {
            int coordinateToUse;
            if (SquareSection.UP.equals(squareSection) || SquareSection.DOWN.equals(squareSection)) {
                coordinateToUse = point.y;
            } else {
                coordinateToUse = point.x;
            }

            List<Point> alignedPoints = alignedPointsMap.computeIfAbsent(coordinateToUse, k -> new ArrayList<>());
            alignedPoints.add(point);
        }

        return alignedPointsMap;
    }
}
