package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.BasicImageAnalyser;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;

import java.awt.image.BufferedImage;

public abstract class AbstractAnalyser<T> {
    protected BasicImageAnalyser basicImageAnalyser;
    protected TotalLumaPixelCalculationsHelper calculationsHelper;

    public AbstractAnalyser() {
        this.basicImageAnalyser = new BasicImageAnalyser(TotalLumaPixelCalculationsHelper.getInstance());
        this.calculationsHelper = TotalLumaPixelCalculationsHelper.getInstance();
    }

    public abstract T analyze(BufferedImage bufferedImage);
}
