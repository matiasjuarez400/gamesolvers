package mathecat.block.puzzleimagesolver.processing.imageanalysis;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.puzzleimagesolver.model.Pair;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.model.PositionedPixel;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.PixelCalculationsHelper;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;
import mathecat.block.puzzleimagesolver.processing.utils.BrigthnessVariationRangeCalculator;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class BasicImageAnalyser {
    public enum SearchDirection {
        HORIZONTAL, VERTICAL
    }

    private PixelCalculationsHelper pixelCalculationsHelper;

    public BasicImageAnalyser(PixelCalculationsHelper pixelCalculationsHelper) {
        this.pixelCalculationsHelper = pixelCalculationsHelper;
    }

    public BasicImageAnalyser() {
        this(TotalLumaPixelCalculationsHelper.getInstance());
    }

    public List<Point> findZonesWithBrightnessVariationInRange(BufferedImage bufferedImage,
                                                        double minVariation, double maxVariation,
                                                        int pixelsToAnalyzeTogether, SearchDirection searchDirection) {
        double desiredVariation = (maxVariation + minVariation) / 2;
        double tolerance = maxVariation - desiredVariation;

        return findZonesWithBrightnessVariation(bufferedImage, desiredVariation, tolerance, pixelsToAnalyzeTogether, searchDirection);
    }

    public List<Point> findZonesWithBrightnessVariation(BufferedImage bufferedImage, double desiredVariation,
                                                        double tolerance, int pixelsToAnalyzeTogether,
                                                        SearchDirection searchDirection) {
        List<Point> pointsFound = new ArrayList<>();

        for (int y = 0; y < bufferedImage.getHeight(); y++) {
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                List<PositionedPixel> pixels = grabPixels(bufferedImage, new Point(x, y), pixelsToAnalyzeTogether, searchDirection);
                PositionedPixel p1 = pixels.get(0);

                for (int i = 1; i < pixels.size(); i++) {
                    PositionedPixel p2 = pixels.get(i);

                    double brightnessVariation = pixelCalculationsHelper.
                            calculateBrightnessVariation(p1.getPixel(), p2.getPixel());

                    if (NumericUtils.isNumberAroundValue(brightnessVariation, desiredVariation, tolerance)) {
                        Point middlePoint = p1.getPosition().
                                calculateMiddlePoint(p2.getPosition()
                        );
                        pointsFound.add(middlePoint);
                    }
                }
            }
        }

        return pointsFound;
    }

    public List<PositionedPixel> extractPixelsInLineHorizontally(BufferedImage image, int y) {
        return grabPixels(image, new Point(0, y), image.getWidth(), SearchDirection.HORIZONTAL);
    }

    public int[][] extractPixelsMatrix(BufferedImage bufferedImage) {
        int[][] pixelsMatrix = new int[bufferedImage.getHeight()][bufferedImage.getWidth()];

        for (int y = 0; y < bufferedImage.getHeight(); y++) {
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                pixelsMatrix[y][x] = bufferedImage.getRGB(x, y);
            }
        }

        return pixelsMatrix;
    }

    public List<PositionedPixel> extractPixelsInLine(BufferedImage image, Line line) {
        SearchDirection searchDirection;
        Point initialPoint;
        if (line.isVertical()) {
            searchDirection = SearchDirection.VERTICAL;
            if (line.getStart().y < line.getEnd().y) {
                initialPoint = new Point(line.getStart().x, line.getStart().y);
            } else {
                initialPoint = new Point(line.getStart().x, line.getEnd().y);
            }
        } else if (line.isHorizontal()) {
            searchDirection = SearchDirection.HORIZONTAL;
            if (line.getStart().x < line.getEnd().x) {
                initialPoint = new Point(line.getStart().x, line.getStart().y);
            } else {
                initialPoint = new Point(line.getEnd().x, line.getStart().y);
            }
        } else {
            throw new IllegalArgumentException("Lines should be vertical or horizontal only");
        }

        int howManyPixels = (int) line.calculateLength();

        return grabPixels(image, initialPoint, howManyPixels, searchDirection);
    }

    public List<Pair<PositionedPixel, PositionedPixel>> extractPixelsAroundLine(BufferedImage image, Line line, int distanceAroundLine) {
        Line preLine;
        Line afterLine;

        if (line.isHorizontal()) {
            preLine = line.moveVertically(-distanceAroundLine);
            afterLine = line.moveVertically(distanceAroundLine);
        } else if (line.isVertical()) {
            preLine = line.moveHorizontally(-distanceAroundLine);
            afterLine = line.moveHorizontally(distanceAroundLine);
        } else {
            throw new IllegalArgumentException("Line must be horizontal or vertical");
        }

        List<PositionedPixel> preLimitPixels = extractPixelsInLine(image, preLine);
        List<PositionedPixel> afterLimitPixels = extractPixelsInLine(image, afterLine);

        List<Pair<PositionedPixel, PositionedPixel>> pairs = new ArrayList<>();
        for (int i = 0; i < preLimitPixels.size(); i++) {
            pairs.add(new Pair<>(
                    preLimitPixels.get(i),
                    afterLimitPixels.get(i)
            ));
        }

        return pairs;
    }

    private List<PositionedPixel> grabPixels(BufferedImage bufferedImage, Point initialPoint,
                                             int howManyPixels, SearchDirection searchDirection) {
        Steps steps = new Steps(searchDirection);

        List<PositionedPixel> pixels = new ArrayList<>();

        int cx = initialPoint.x;
        int cy = initialPoint.y;
        while (pixels.size() < howManyPixels && cx < bufferedImage.getWidth() && cy < bufferedImage.getHeight()) {
            pixels.add(new PositionedPixel(new Pixel(bufferedImage.getRGB(cx, cy)), new Point(cx, cy)));
            cx += steps.hStep;
            cy += steps.vStep;
        }

        return pixels;
    }

    public Range calculateZoneBrightnessVariationRange(List<Pair<PositionedPixel, PositionedPixel>> pairs) {
        BrigthnessVariationRangeCalculator calculator = new BrigthnessVariationRangeCalculator();
        for (Pair<PositionedPixel, PositionedPixel> pair : pairs) {
            calculator.addEntry(pair.getLeft().getPixel(), pair.getRight().getPixel());
        }

        return calculator.calculate();
    }

    public Range calculateBrightnessVariationRangeAroundLine(BufferedImage image, Line line, int distanceAroundLine) {
        return calculateZoneBrightnessVariationRange(extractPixelsAroundLine(image, line, distanceAroundLine));
    }

    private class Steps {
        private int hStep;
        private int vStep;

        public Steps(SearchDirection searchDirection) {
            if (searchDirection == SearchDirection.HORIZONTAL) {
                hStep = 1;
                vStep = 0;
            } else if(searchDirection == SearchDirection.VERTICAL) {
                hStep = 0;
                vStep = 1;
            } else {
                throw new IllegalStateException("Search direction not supported");
            }
        }
    }
}
