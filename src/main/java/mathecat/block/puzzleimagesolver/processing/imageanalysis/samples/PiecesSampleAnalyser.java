package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import mathecat.block.puzzleimagesolver.model.ColorRange;
import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PiecesSampleResult;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.StatisticalUtils;

import java.awt.image.BufferedImage;

public class PiecesSampleAnalyser extends AbstractSampleAnalyser<PiecesSampleResult> {
    private static double PIECES_COLOR_DEVIATION_MULTIPLIER = 3;

    @Override
    public PiecesSampleResult analyse() {
        PiecesSampleResult result = new PiecesSampleResult();

        result.setGreenColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_GREEN)));

        result.setLightBlueColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_LIGHTBLUE)));

        result.setPinkColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_PINK)));

        result.setPurpleColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_PURPLE)));

        result.setRedColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_RED)));

        result.setYellowColorRange(extractColorRangeFromSample(
                imageIOHandler.loadImageByResourceName(SampleConstants.PIECES_YELLOW)));

        return result;
    }

    private ColorRange extractColorRangeFromSample(BufferedImage image) {
        int pixelAmount = image.getWidth() * image.getHeight();

        double[] redValues = new double[pixelAmount];
        double[] greenValues = new double[pixelAmount];
        double[] blueValues = new double[pixelAmount];

        int width = image.getWidth();
        int height = image.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Pixel pixel = new Pixel(image.getRGB(x, y));
                redValues[x + y * width] = pixel.getRed();
                greenValues[x + y * width] = pixel.getGreen();
                blueValues[x + y * width] = pixel.getBlue();
            }
        }

        return new ColorRange(
                buildRangeForColor(redValues),
                buildRangeForColor(greenValues),
                buildRangeForColor(blueValues)
        );
    }

    private Range buildRangeForColor(double[] colorValues) {
        double avg = StatisticalUtils.average(colorValues);
        double deviation = StatisticalUtils.standardDeviation(colorValues) * PIECES_COLOR_DEVIATION_MULTIPLIER;
        double upperLimit = avg + deviation;
        double lowerLimit = avg - deviation;

        return new Range(lowerLimit, upperLimit, avg, deviation);
    }
}
