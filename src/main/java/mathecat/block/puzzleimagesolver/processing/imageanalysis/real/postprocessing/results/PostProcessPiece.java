package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results;

import mathecat.block.puzzleimagesolver.model.ImageBlock;

import java.util.ArrayList;
import java.util.List;

public class PostProcessPiece {
    private boolean[][] model;
    private List<ImageBlock> blocks;

    public PostProcessPiece() {
        this.blocks = new ArrayList<>();
    }

    public boolean[][] getModel() {
        return model;
    }

    public void setModel(boolean[][] model) {
        this.model = model;
    }

    public List<ImageBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<ImageBlock> blocks) {
        this.blocks = blocks;
    }

    public void addBlock(ImageBlock imageBlock) {
        this.blocks.add(imageBlock);
    }
}
