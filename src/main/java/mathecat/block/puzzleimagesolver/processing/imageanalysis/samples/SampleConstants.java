package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.resolutionlogic.model.Point;

public class SampleConstants {
    // These three values are used to teach the program where to look for regions limits in the images
    // We are assuming that all images will be similar to samples/puzzle.png
    public static final String PUZZLE_SAMPLE_SOURCE = "/samples/puzzle.png";
    public static final int PUZZLE_SAMPLE_BOARD_UPPER_LIMIT_POSITION = 106;
    public static final int PUZZLE_SAMPLE_BOARD_LOWER_LIMIT_POSITION = 872;

    public static final String PIECES_GREEN = "/samples/piece_green.png";
    public static final String PIECES_LIGHTBLUE = "/samples/piece_lightblue.png";
    public static final String PIECES_PINK = "/samples/piece_pink.png";
    public static final String PIECES_PURPLE = "/samples/piece_purple.png";
    public static final String PIECES_RED = "/samples/piece_red.png";
    public static final String PIECES_YELLOW = "/samples/piece_yellow.png";

    public static final String BOARD_SQUARE = "/samples/boardSquare.png";
    public static final Line BOARD_SQUARE_UPPER_LIMIT = new Line(
            new Point(8, 3), new Point(82, 3)
    );
    public static final Line BOARD_SQUARE_BOTTOM_LIMIT = new Line(
            new Point(8, 87), new Point(82, 87)
    );
    public static final Line BOARD_SQUARE_LEFT_LIMIT = new Line(
            new Point(3, 8), new Point(3, 82)
    );
    public static final Line BOARD_SQUARE_RIGHT_LIMIT = new Line(
            new Point(87, 8), new Point(87, 82)
    );
}
