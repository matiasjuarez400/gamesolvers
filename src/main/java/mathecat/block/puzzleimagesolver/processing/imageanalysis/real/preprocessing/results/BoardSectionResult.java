package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results;

import mathecat.block.resolutionlogic.model.Point;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class BoardSectionResult {
    private List<Point> leftPoints;
    private List<Point> rightPoints;
    private List<Point> upPoints;
    private List<Point> downPoints;
    private BufferedImage originalImage;

    public BoardSectionResult() {
        leftPoints = new ArrayList<>();
        rightPoints = new ArrayList<>();
        upPoints = new ArrayList<>();
        downPoints = new ArrayList<>();
    }

    public List<Point> getLeftPoints() {
        return leftPoints;
    }

    public List<Point> getRightPoints() {
        return rightPoints;
    }

    public List<Point> getUpPoints() {
        return upPoints;
    }

    public List<Point> getDownPoints() {
        return downPoints;
    }

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }
}
