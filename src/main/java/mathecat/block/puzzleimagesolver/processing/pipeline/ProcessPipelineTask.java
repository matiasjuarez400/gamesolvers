package mathecat.block.puzzleimagesolver.processing.pipeline;

public interface ProcessPipelineTask {
    void execute(ProcessPipelineResult result);
}
