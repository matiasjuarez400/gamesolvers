package mathecat.block.puzzleimagesolver.processing.pipeline;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PiecesSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PuzzleRegionsResult;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.result.BoardResolutionModelConvertionResult;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.result.PieceResolutionModelConvertionResult;
import mathecat.block.resolutionlogic.model.PuzzleSolution;

import java.awt.image.BufferedImage;

public class ProcessPipelineResult {
    private BufferedImage originalImage;
    private PuzzleRegionsResult puzzleRegionsResult;
    private PiecesSectionResult piecesSectionResult;
    private BoardSectionResult boardSectionResult;

    private PiecesSectionPostProcessingResult piecesSectionPostProcessingResult;
    private BoardSectionPostProcessingResult boardSectionPostProcessingResult;

    private PieceResolutionModelConvertionResult pieceResolutionModelConvertionResult;
    private BoardResolutionModelConvertionResult boardResolutionModelConvertionResult;

    private PuzzleSolution puzzleSolution;
    private BufferedImage solutionImage;
    private String solutionImageBase64;

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }

    public PuzzleRegionsResult getPuzzleRegionsResult() {
        return puzzleRegionsResult;
    }

    public void setPuzzleRegionsResult(PuzzleRegionsResult puzzleRegionsResult) {
        this.puzzleRegionsResult = puzzleRegionsResult;
    }

    public PiecesSectionResult getPiecesSectionResult() {
        return piecesSectionResult;
    }

    public void setPiecesSectionResult(PiecesSectionResult piecesSectionResult) {
        this.piecesSectionResult = piecesSectionResult;
    }

    public BoardSectionResult getBoardSectionResult() {
        return boardSectionResult;
    }

    public void setBoardSectionResult(BoardSectionResult boardSectionResult) {
        this.boardSectionResult = boardSectionResult;
    }

    public PiecesSectionPostProcessingResult getPiecesSectionPostProcessingResult() {
        return piecesSectionPostProcessingResult;
    }

    public void setPiecesSectionPostProcessingResult(PiecesSectionPostProcessingResult piecesSectionPostProcessingResult) {
        this.piecesSectionPostProcessingResult = piecesSectionPostProcessingResult;
    }

    public BoardSectionPostProcessingResult getBoardSectionPostProcessingResult() {
        return boardSectionPostProcessingResult;
    }

    public void setBoardSectionPostProcessingResult(BoardSectionPostProcessingResult boardSectionPostProcessingResult) {
        this.boardSectionPostProcessingResult = boardSectionPostProcessingResult;
    }

    public PieceResolutionModelConvertionResult getPieceResolutionModelConvertionResult() {
        return pieceResolutionModelConvertionResult;
    }

    public void setPieceResolutionModelConvertionResult(PieceResolutionModelConvertionResult pieceResolutionModelConvertionResult) {
        this.pieceResolutionModelConvertionResult = pieceResolutionModelConvertionResult;
    }

    public BoardResolutionModelConvertionResult getBoardResolutionModelConvertionResult() {
        return boardResolutionModelConvertionResult;
    }

    public void setBoardResolutionModelConvertionResult(BoardResolutionModelConvertionResult boardResolutionModelConvertionResult) {
        this.boardResolutionModelConvertionResult = boardResolutionModelConvertionResult;
    }

    public PuzzleSolution getPuzzleSolution() {
        return puzzleSolution;
    }

    public void setPuzzleSolution(PuzzleSolution puzzleSolution) {
        this.puzzleSolution = puzzleSolution;
    }

    public BufferedImage getSolutionImage() {
        return solutionImage;
    }

    public void setSolutionImage(BufferedImage solutionImage) {
        this.solutionImage = solutionImage;
    }

    public String getSolutionImageBase64() {
        return solutionImageBase64;
    }

    public void setSolutionImageBase64(String solutionImageBase64) {
        this.solutionImageBase64 = solutionImageBase64;
    }
}
