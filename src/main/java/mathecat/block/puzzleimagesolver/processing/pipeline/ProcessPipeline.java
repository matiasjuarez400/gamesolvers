package mathecat.block.puzzleimagesolver.processing.pipeline;

import mathecat.block.puzzleimagesolver.processing.ImageBase64Converter;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.BoardSectionMainPostProcessor;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.PiecesSectionResultPostProcessor;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.BoardSectionAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.PiecesSectionAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.PuzzleRegionsAnalyser;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.BoardResolutionModelConverter;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.PieceResolutionModelConverter;
import mathecat.block.resolutionlogic.BlockSolver;
import mathecat.block.resolutionlogic.PuzzleSolutionImageCreator;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class ProcessPipeline {
    private List<ProcessPipelineTask> tasks = new ArrayList<>();
    private PuzzleRegionsAnalyser puzzleRegionsAnalyser;
    private PiecesSectionAnalyser piecesSectionAnalyser;
    private BoardSectionAnalyser boardSectionAnalyser;

    private PiecesSectionResultPostProcessor piecesSectionResultPostProcessor;
    private BoardSectionMainPostProcessor boardSectionMainPostProcessor;

    private PieceResolutionModelConverter pieceResolutionModelConverter;
    private BoardResolutionModelConverter boardResolutionModelConverter;

    private BlockSolver blockSolver;
    private PuzzleSolutionImageCreator puzzleSolutionImageCreator;
    private ImageBase64Converter imageBase64Converter;

    public ProcessPipeline() {
        // PREPROCESSING
        puzzleRegionsAnalyser = new PuzzleRegionsAnalyser();
        piecesSectionAnalyser = new PiecesSectionAnalyser();
        boardSectionAnalyser = new BoardSectionAnalyser();

        // POSTPROCESSING
        piecesSectionResultPostProcessor = new PiecesSectionResultPostProcessor();
        boardSectionMainPostProcessor = new BoardSectionMainPostProcessor();

        // MODEL CONVERTION
        pieceResolutionModelConverter = new PieceResolutionModelConverter();
        boardResolutionModelConverter = new BoardResolutionModelConverter();

        // PUZZLE SOLUTION
        blockSolver = new BlockSolver();
        puzzleSolutionImageCreator = new PuzzleSolutionImageCreator();
        imageBase64Converter = new ImageBase64Converter();

        addTasks();
    }

    private void addTasks() {
        // PREPROCESSING
        tasks.add(result -> result.setPuzzleRegionsResult(puzzleRegionsAnalyser.analyze(result.getOriginalImage())));

        tasks.add(result -> result.setPiecesSectionResult(
                piecesSectionAnalyser.analyze(result.getPuzzleRegionsResult().getPiecesSectionImage())
        ));

        tasks.add(result -> result.setBoardSectionResult(
                boardSectionAnalyser.analyze(result.getPuzzleRegionsResult().getBoardSectionImage())
        ));

        // POSTPROCESSING
        tasks.add(result -> result.setPiecesSectionPostProcessingResult(
                piecesSectionResultPostProcessor.analyse(result.getPiecesSectionResult())
        ));

        tasks.add(result -> result.setBoardSectionPostProcessingResult(
                boardSectionMainPostProcessor.analyse(result.getBoardSectionResult())
        ));


        // MODEL CONVERTION
        tasks.add(result -> result.setPieceResolutionModelConvertionResult(
                pieceResolutionModelConverter.convert(result.getPiecesSectionPostProcessingResult())
        ));

        tasks.add(result -> result.setBoardResolutionModelConvertionResult(
                boardResolutionModelConverter.convert(result.getBoardSectionPostProcessingResult())
        ));


        // PUZZLE SOLUTION
        tasks.add(result -> result.setPuzzleSolution(
                blockSolver.solve(
                        result.getBoardResolutionModelConvertionResult().getBoard(),
                        result.getPieceResolutionModelConvertionResult().getPieces()
                )
        ));

        tasks.add(result -> result.setSolutionImage(
                puzzleSolutionImageCreator.drawSolution(result.getPuzzleSolution())
        ));

        tasks.add(result -> result.setSolutionImageBase64(
                imageBase64Converter.convert(result.getSolutionImage())
        ));
    }

    public ProcessPipelineResult analyse(BufferedImage image) {
        ProcessPipelineResult result = new ProcessPipelineResult();
        result.setOriginalImage(image);

        for (ProcessPipelineTask task : this.tasks) {
            try {
                task.execute(result);
            } catch (Exception e) {
                break;
            }
        }

        return result;
    }
}
