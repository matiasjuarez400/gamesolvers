package mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation;

import mathecat.block.puzzleimagesolver.model.Pixel;

public class LumaDifferenceCalculator extends BrightnessCalculator {
    @Override
    public double calculateBrightnessVariation(Pixel p1, Pixel p2) {
        double startingLuma = calculateLuma(p1);
        double endLuma = calculateLuma(p2);

        return endLuma - startingLuma;
    }
}
