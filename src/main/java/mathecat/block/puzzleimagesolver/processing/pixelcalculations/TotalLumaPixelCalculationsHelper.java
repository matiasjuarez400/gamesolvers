package mathecat.block.puzzleimagesolver.processing.pixelcalculations;

import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.TotalLumaProportionCalculator;

public class TotalLumaPixelCalculationsHelper extends PixelCalculationsHelper {
    private static TotalLumaPixelCalculationsHelper instance;

    private TotalLumaPixelCalculationsHelper() {
        super(new TotalLumaProportionCalculator());
    }

    public static TotalLumaPixelCalculationsHelper getInstance() {
        if (instance == null) {
            instance = new TotalLumaPixelCalculationsHelper();
        }
        return instance;
    }
}
