package mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation;

import mathecat.block.puzzleimagesolver.model.Pixel;

public class LumaProportionCalculator extends BrightnessCalculator {
    @Override
    public double calculateBrightnessVariation(Pixel p1, Pixel p2) {
        double initialLuma = super.calculateLuma(p1);
        double endLuma = super.calculateLuma(p2);

        return (endLuma - initialLuma) / (initialLuma);
    }
}
