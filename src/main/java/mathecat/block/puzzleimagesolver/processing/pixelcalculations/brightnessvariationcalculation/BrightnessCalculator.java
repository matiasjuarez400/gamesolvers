package mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation;

import mathecat.block.puzzleimagesolver.model.Pixel;

public abstract class BrightnessCalculator {
    public abstract double calculateBrightnessVariation(Pixel p1, Pixel p2);

    public double calculateBrightness(Pixel p1) {
        return calculateLuma(p1);
    }

    protected double calculateLuma(Pixel p) {
        return 0.299 * p.getRed() + 0.587 * p.getGreen() + 0.114 * p.getBlue();
    }
}
