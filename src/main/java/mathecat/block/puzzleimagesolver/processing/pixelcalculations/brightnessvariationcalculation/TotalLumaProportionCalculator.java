package mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation;

import mathecat.block.puzzleimagesolver.model.Pixel;

public class TotalLumaProportionCalculator extends BrightnessCalculator {
    private LumaDifferenceCalculator lumaDifferenceCalculator;

    public TotalLumaProportionCalculator() {
        this.lumaDifferenceCalculator = new LumaDifferenceCalculator();
    }

    @Override
    public double calculateBrightnessVariation(Pixel p1, Pixel p2) {
        double lumaDifference = lumaDifferenceCalculator.calculateBrightnessVariation(p1, p2);
        double totalLuma = super.calculateLuma(p1) + super.calculateLuma(p2);

        if (totalLuma == 0) return 0.0;

        return lumaDifference / totalLuma;
    }
}
