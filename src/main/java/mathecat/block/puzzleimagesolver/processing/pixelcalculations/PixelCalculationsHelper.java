package mathecat.block.puzzleimagesolver.processing.pixelcalculations;

import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.BrightnessCalculator;

public class PixelCalculationsHelper {
    private BrightnessCalculator brightnessCalculator;

    public PixelCalculationsHelper(BrightnessCalculator brightnessCalculator) {
        this.brightnessCalculator = brightnessCalculator;
    }

    public double calculateColorDistance(Pixel p1, Pixel p2) {
        return Math.sqrt(
                Math.pow(p1.getRed() - p2.getRed(), 2) +
                        Math.pow(p1.getGreen() - p2.getGreen(), 2) +
                        Math.pow(p1.getBlue() - p2.getBlue(), 2));
    }

    public double calculateBrightnessVariation(Pixel p1, Pixel p2) {
        return this.brightnessCalculator.calculateBrightnessVariation(p1, p2);
    }

    public double calculateBrightness(Pixel p1) {
        return this.brightnessCalculator.calculateBrightness(p1);
    }
}
