package mathecat.block.puzzleimagesolver.processing.modelconvertion.result;

import mathecat.block.resolutionlogic.model.Piece;

import java.util.ArrayList;
import java.util.List;

public class PieceResolutionModelConvertionResult {
    private List<Piece> pieces = new ArrayList<>();

    public List<Piece> getPieces() {
        return pieces;
    }
}
