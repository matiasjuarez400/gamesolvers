package mathecat.block.puzzleimagesolver.processing.modelconvertion;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.result.BoardResolutionModelConvertionResult;
import mathecat.block.resolutionlogic.model.BlockType;
import mathecat.block.resolutionlogic.model.Board;

public class BoardResolutionModelConverter {
    public BoardResolutionModelConvertionResult convert(BoardSectionPostProcessingResult boardSectionPostProcessingResult) {
        Board board = new Board();

        boolean[][] boardModel = boardSectionPostProcessingResult.getModel();

        for (int y = 0; y < boardModel.length; y++) {
            BlockType[] newRow = new BlockType[boardModel[y].length];

            for (int x = 0; x < boardModel[y].length; x++) {
                if (boardModel[y][x]) {
                    newRow[x] = BlockType.E;
                } else {
                    newRow[x] = BlockType.X;
                }
            }

            board.addRow(newRow);
        }

        BoardResolutionModelConvertionResult resolutionModelConvertionResult = new BoardResolutionModelConvertionResult();
        resolutionModelConvertionResult.setBoard(board);

        return resolutionModelConvertionResult;
    }
}
