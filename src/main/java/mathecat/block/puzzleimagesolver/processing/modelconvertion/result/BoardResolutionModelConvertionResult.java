package mathecat.block.puzzleimagesolver.processing.modelconvertion.result;

import mathecat.block.resolutionlogic.model.Board;

public class BoardResolutionModelConvertionResult {
    private Board board;

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
}
