package mathecat.block.puzzleimagesolver.processing.modelconvertion;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PiecesSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PostProcessPiece;
import mathecat.block.puzzleimagesolver.processing.modelconvertion.result.PieceResolutionModelConvertionResult;
import mathecat.block.resolutionlogic.model.BlockType;
import mathecat.block.resolutionlogic.model.Piece;

public class PieceResolutionModelConverter {
    public Piece convert(PostProcessPiece postProcessPiece) {
        Piece piece = new Piece();
        boolean[][] pieceModel = postProcessPiece.getModel();

        for (int y = 0; y < pieceModel.length; y++) {
            BlockType[] newRow = new BlockType[pieceModel[y].length];

            for (int x = 0; x < pieceModel[y].length; x++) {
                if (pieceModel[y][x]) {
                    newRow[x] = BlockType.F;
                } else {
                    newRow[x] = BlockType.E;
                }
            }

            piece.addRow(newRow);
        }

        return piece;
    }

    public PieceResolutionModelConvertionResult convert(PiecesSectionPostProcessingResult piecesSectionPostProcessingResult) {
        PieceResolutionModelConvertionResult result = new PieceResolutionModelConvertionResult();

        for (PostProcessPiece postProcessPiece : piecesSectionPostProcessingResult.getPostProcessPieces()) {
            result.getPieces().add(convert(postProcessPiece));
        }

        return result;
    }
}
