package mathecat.block.puzzleimagesolver.processing.utils;

import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.StatisticalUtils;
import mathecat.block.resolutionlogic.model.Point;

import java.util.List;

public class NumericUtils {
    public static boolean isNumberInRange(double number, double minValue, double maxValue) {
        return ((number >= minValue) && number <= maxValue);
    }

    public static boolean isNumberAroundValue(double number, double value, double limit) {
        return isNumberInRange(number, value - limit, value + limit);
    }

    public static boolean isNumberInRange(double number, Range range) {
        boolean usingRange = isNumberInRange(number, range.lowerLimit, range.upperLimit);
        boolean usingMiddle = isNumberAroundValue(number, range.center, range.tolerance);

//        if (usingMiddle ^ usingRange) {
//            throw new IllegalStateException("The Range parameter is malformed");
//        }

        return usingMiddle || usingRange;
    }

    public static Point calculateRangeAvg(List<Point> pointList) {
        int xAcc = 0;
        int yAcc = 0;

        for (Point point : pointList) {
            xAcc += point.x;
            yAcc += point.y;
        }

        Point center = new Point(xAcc / pointList.size(), yAcc / pointList.size());

        return center;
    }

    public static Point calculateRangeCenter(List<Point> pointList) {
        double[] xValues = new double[pointList.size()];
        double[] yValues = new double[pointList.size()];

        for (int i = 0; i < pointList.size(); i++) {
            Point current = pointList.get(i);
            xValues[i] = current.x;
            yValues[i] = current.y;
        }

        double xCenter = calculateRangeCenter(xValues);
        double yCenter = calculateRangeCenter(yValues);

        return new Point((int)xCenter, (int)yCenter);
    }

    public static double calculateMin(double... numbers) {
        double min = numbers[0];

        for (Double number : numbers) {
            if (number < min) {
                min = number;
            }
        }

        return min;
    }

    public static double calculateMax(double... numbers) {
        double max = numbers[0];

        for (Double number : numbers) {
            if (number > max) {
                max = number;
            }
        }

        return max;
    }

    public static double calculateMaxDistanceFromAvg(double... numbers) {
        double avg = StatisticalUtils.average(numbers);
        double max = calculateMax(numbers);
        double min = calculateMin(numbers);

        double minDistance = Math.abs(min - avg);
        double maxDistance = Math.abs(max - avg);

        return minDistance < maxDistance ? maxDistance : minDistance;
    }

    public static double calculateRangeCenter(double... numbers) {
        double max = calculateMax(numbers);
        double min = calculateMin(numbers);

        return (max + min) / 2.0;
    }

    public static double calculateMaxDistanceToRangeCenter(double... numbers) {
        double max = calculateMax(numbers);
        double center = calculateRangeCenter(numbers);

        return max - center;
    }
}
