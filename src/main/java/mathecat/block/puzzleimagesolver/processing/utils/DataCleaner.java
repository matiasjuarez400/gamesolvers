package mathecat.block.puzzleimagesolver.processing.utils;

import mathecat.block.resolutionlogic.model.Point;

import java.util.*;

public class DataCleaner {
    public static List<Point> keepPointsConcentratedVertically(List<Point> points, int verticalTolerance) {
        List<Point> cleanedPoints = new ArrayList<>(points);
        if (cleanedPoints.isEmpty()) return cleanedPoints;

        Map<Integer, Integer> counterByYPosition = new HashMap<>();

        for (Point point : points) {
            int y = point.y;

            boolean foundKeyInMap = false;
            for (Map.Entry<Integer, Integer> entry : counterByYPosition.entrySet()) {
                Integer key = entry.getKey();

                if (NumericUtils.isNumberAroundValue(y, key, verticalTolerance)) {
                    entry.setValue(entry.getValue() + 1);
                    foundKeyInMap = true;
                    break;
                }
            }

            if (!foundKeyInMap) {
                counterByYPosition.put(y, 1);
            }
        }

        Map.Entry<Integer, Integer> biggerCounter = null;
        for (Map.Entry<Integer, Integer> entry : counterByYPosition.entrySet()) {
            if (biggerCounter == null) {
                biggerCounter = entry;
            } else {
                if (entry.getValue() > biggerCounter.getValue()) {
                    biggerCounter = entry;
                }
            }
        }

        // CLEANING PROCESS
        Iterator<Point> pointIterator = cleanedPoints.iterator();
        while (pointIterator.hasNext()) {
            Point point = pointIterator.next();

            if (!NumericUtils.isNumberAroundValue(point.y, biggerCounter.getKey(), verticalTolerance)) {
                pointIterator.remove();
            }
        }

        return cleanedPoints;
    }
}
