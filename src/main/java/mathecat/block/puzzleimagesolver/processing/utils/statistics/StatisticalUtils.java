package mathecat.block.puzzleimagesolver.processing.utils.statistics;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatisticalUtils {
    public static double median(double... numbers) {
        List<Double> numbersList = new ArrayList<>();
        for (Double number : numbers) {
            numbersList.add(number);
        }
        return median(numbersList);
    }

    public static double median(List<Double> numbers) {
        if (CollectionUtils.isEmpty(numbers)) {
            throw new IllegalArgumentException("numbers list can not be empty");
        }

        List<Double> innerList = new ArrayList<>(numbers);

        sort(innerList);

        int numbersize = innerList.size();
        int middlePosition = numbersize / 2;
        if (numbersize % 2 == 0 && numbersize > 0) {
            double firstValue = innerList.get(middlePosition - 1);
            double secondValue = innerList.get(middlePosition);

            return (firstValue + secondValue) / 2;
        } else {
            return innerList.get(middlePosition);
        }
    }

    public static double average(double... numbers) {
        List<Double> numberList = new ArrayList<>();
        for (Double number : numbers) {
            numberList.add(number);
        }

        return average(numberList);
    }

    public static double average(List<Double> numbers) {
        double acc = 0;

        for (Double number : numbers) {
            acc += number;
        }

        return acc / numbers.size();
    }

    public static double standardDeviation(double... numbers) {
        List<Double> numberList = new ArrayList<>();
        for (Double number : numbers) {
            numberList.add(number);
        }

        return standardDeviation(numberList);
    }

    public static double standardDeviation(List<Double> numbers) {
        double avg = StatisticalUtils.average(numbers);

        double varianceAcc = 0;
        for (Double number : numbers) {
            varianceAcc += Math.pow((number - avg), 2);
        }

        return Math.sqrt(varianceAcc / numbers.size());
    }

    private static void sort(List<Double> numbers) {
        Collections.sort(numbers, (n1, n2) -> {
            double diff = n1 - n2;
            if (diff < 0) return -1;
            if (diff == 0) return 0;
            return 1;
        });
    }
}
