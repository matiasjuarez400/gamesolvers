package mathecat.block.puzzleimagesolver.processing.utils;

import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BrigthnessVariationRangeCalculator {
    private List<Map.Entry<Pixel, Pixel>> pixelEntries = new ArrayList<>();
    private TotalLumaPixelCalculationsHelper calculationsHelper;

    public BrigthnessVariationRangeCalculator() {
        this.calculationsHelper = TotalLumaPixelCalculationsHelper.getInstance();
    }

    public void addEntry(Pixel p1, Pixel p2) {
        pixelEntries.add(new AbstractMap.SimpleImmutableEntry<>(p1, p2));
    }

    private double[] calculateWorkingValues() {
        double[] values = new double[pixelEntries.size()];
        for (int i = 0; i < values.length; i++) {
            Map.Entry<Pixel, Pixel> entry = pixelEntries.get(i);
            values[i] = (calculationsHelper.calculateBrightnessVariation(entry.getKey(), entry.getValue()));
        }

        return values;
    }

    public Range calculate() {
        double[] values = calculateWorkingValues();
        return new Range(
                NumericUtils.calculateMin(values),
                NumericUtils.calculateMax(values),
                NumericUtils.calculateRangeCenter(values),
                NumericUtils.calculateMaxDistanceToRangeCenter(values)
        );
    }
}
