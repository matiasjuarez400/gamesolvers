package mathecat.block.puzzleimagesolver.processing.utils.statistics;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class AnomalyDataKit {
    private static double PROPORTION_TO_REMOVE = 0.20;

    private double Q1;
    private double Q2;
    private double Q3;
    private double quartileRange;
    private double lowerInnerLimit;
    private double upperInnerLimit;
    private double lowerOuterLimit;
    private double upperOuterLimit;
    private double trunkedAvg;
    private double trunkedStandardDeviation;

    private AnomalyDataKit() {}

    public static AnomalyDataKit create(double... numbers) {
        List<Double> numbersList = new ArrayList<>();
        for (Double number : numbers) {
            numbersList.add(number);
        }

        return create(numbersList);
    }

    public static AnomalyDataKit create(List<Double> numbers) {
        List<Double> sortedList = new ArrayList<>(numbers);

        Collections.sort(sortedList, (n1, n2) -> {
            double diff = n1 - n2;
            if (diff < 0) return -1;
            if (diff == 0) return 0;
            return 1;
        });

        AnomalyDataKit anomalyDataKit = new AnomalyDataKit();
        anomalyDataKit.initQuartilInformation(sortedList);
        anomalyDataKit.calculateTrunkedIndicators(sortedList);

        return anomalyDataKit;
    }

    private void calculateTrunkedIndicators(List<Double> sortedList) {
        List<Double> cleanedList = new ArrayList<>(sortedList);

        int trunkSize = (int) ((cleanedList.size() * PROPORTION_TO_REMOVE) / 2);

        if (trunkSize == 0 && trunkSize < cleanedList.size()) {
            trunkSize = 2;
        }

        for (int i = 0; i < trunkSize; i++) {
            cleanedList.remove(0);
            cleanedList.remove(cleanedList.size() - 1);
        }

        this.trunkedAvg = StatisticalUtils.average(cleanedList);
        this.trunkedStandardDeviation = StatisticalUtils.standardDeviation(cleanedList);
    }

    private void initQuartilInformation(List<Double> sortedList) {
        int middlePoint = sortedList.size() / 2;

        this.Q1 = StatisticalUtils.median(sortedList.subList(0, middlePoint));
        this.Q2 = StatisticalUtils.median(sortedList);
        this.Q3 = StatisticalUtils.median(sortedList.subList(middlePoint, sortedList.size()));

        quartileRange = Q3 - Q1;

        lowerInnerLimit = Q1 - quartileRange * 1.5;
        upperInnerLimit = Q3 + quartileRange * 1.5;

        lowerOuterLimit = Q1 - quartileRange * 3;
        upperOuterLimit = Q3 + quartileRange * 3;
    }
}
