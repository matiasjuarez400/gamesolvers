package mathecat.block.puzzleimagesolver.processing;

public class ProcessingParameters {
    public float BoardPostResultCleanerByAmountOfPointsInLevel_MinimumPercentageTolerance = 0.8f;

    private ProcessingParameters() {}

    private static ProcessingParameters instance;

    public static ProcessingParameters getInstance() {
        if (instance == null) {
            instance = new ProcessingParameters();
        }

        return instance;
    }
}
