package mathecat.block.puzzleimagesolver.model;

public class ColorRange {
    private Range red;
    private Range green;
    private Range blue;

    public ColorRange(Range red, Range green, Range blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Range getRed() {
        return red;
    }

    public Range getGreen() {
        return green;
    }

    public Range getBlue() {
        return blue;
    }

    public boolean isInRange(Pixel pixel) {
        return red.isInRange(pixel.getRed()) && green.isInRange(pixel.getGreen()) && blue.isInRange(pixel.getBlue());
    }
}
