package mathecat.block.puzzleimagesolver.model;

import mathecat.block.resolutionlogic.model.Point;

public class PositionedPixel {
    private Pixel pixel;
    private Point position;

    public PositionedPixel(int rgb, int x, int y) {
        this(new Pixel(rgb), new Point(x, y));
    }

    public PositionedPixel(Pixel pixel, Point position) {
        this.pixel = pixel;
        this.position = position;
    }

    public Pixel getPixel() {
        return pixel;
    }

    public Point getPosition() {
        return position;
    }
}
