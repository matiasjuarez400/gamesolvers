package mathecat.block.puzzleimagesolver.model;

import mathecat.block.resolutionlogic.model.Point;

public class Line {
    private final Point start;
    private final Point end;

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    public Line(int ix, int iy, int ex, int ey) {
        this(new Point(ix, iy), new Point(ex, ey));
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    public double calculateLength() {
        double xl = end.x - start.x;
        double yl = end.y - start.y;

        return Math.sqrt(Math.pow(xl, 2) + Math.pow(yl, 2));
    }

    public boolean isHorizontal() {
        return start.y == end.y;
    }

    public boolean isVertical() {
        return start.x == end.x;
    }

    public Line moveVertically(int value) {
        return new Line(
                this.start.moveVertically(value),
                this.end.moveVertically(value)
        );
    }

    public Line moveHorizontally(int value) {
        return new Line(
                this.start.moveHorizontally(value),
                this.end.moveHorizontally(value)
        );
    }
}
