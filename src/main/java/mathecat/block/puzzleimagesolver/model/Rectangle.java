package mathecat.block.puzzleimagesolver.model;

import mathecat.block.resolutionlogic.model.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Rectangle {
    private Point p1;
    private Point p2;

    public Rectangle(int ix, int iy, int ex, int ey) {
        this(new Point(ix, iy), new Point(ex, ey));
    }

    public Rectangle(Point p1, Point p2) {
        if (p1.x >= p2.x || p1.y >= p2.y) {
            throw new IllegalArgumentException("Point p2 has to be right-down relative to point p1");
        }

        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getUpperLeftVertex() {
        return p1;
    }

    public Point getUpperRightVertex() {
        return new Point(p2.x, p1.y);
    }

    public Point getLowerLeftVertex() {
        return new Point(p1.x, p2.y);
    }

    public Point getLowerRightVertex() {
        return p2;
    }

    public int getWidth() {
        return Math.abs(p2.x - p1.x);
    }

    public int getHeight() {
        return Math.abs(p2.y - p1.y);
    }

    public Point calculateCenter() {
        return p2.calculateMiddlePoint(p1);
    }

    public int getArea() {
        return getWidth() * getHeight();
    }

    public boolean containsPoint(int x, int y) {
        return (x >= this.p1.x && x <= this.p2.x) && (y >= this.p1.y && y <= this.p2.y);
    }

    public boolean containsPoint(Point point) {
        return containsPoint(point.x, point.y);
    }

    public boolean overlapsWith(Rectangle anotherRectangle) {
        if (this.p1.x > anotherRectangle.p2.x || this.p2.x < anotherRectangle.p1.x) return false;
        if (this.p1.y > anotherRectangle.p2.y || this.p2.y < anotherRectangle.p1.y) return false;

        return true;
    }

    public double getWidthHeightRatio() {
        return getWidth() / ((double) getHeight());
    }

    public List<Point> getVertex() {
        List<Point> vertex = new ArrayList<>();
        Collections.addAll(
                vertex,
                getUpperLeftVertex(),
                getUpperRightVertex(),
                getLowerLeftVertex(),
                getLowerRightVertex()
        );

        return vertex;
    }

    public Double calculateClosestVertexDistance(Rectangle anotherRectangle) {
        Double lessDistance = null;

        for (Point vertex : this.getVertex()) {
            for (Point anotherVertex : anotherRectangle.getVertex()) {
                if (lessDistance == null) {
                    lessDistance = vertex.calculateDistance(anotherVertex);
                } else {
                    double tempDistance = vertex.calculateDistance(anotherVertex);
                    if (tempDistance < lessDistance) {
                        lessDistance = tempDistance;
                    }
                }
            }
        }

        return lessDistance;
    }
}
