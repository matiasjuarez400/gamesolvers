package mathecat.block.puzzleimagesolver.model;

public class Pixel {
    private final int alpha;
    private final int red;
    private final int green;
    private final int blue;

    public Pixel(int pixel) {
        alpha = (pixel >> 24) & 0xff;
        red = (pixel >> 16) & 0xff;
        green = (pixel >> 8) & 0xff;
        blue = pixel & 0xff;
    }

    public Pixel(int r, int g, int b) {
        this.alpha = 0;
        this.red = r;
        this.green = g;
        this.blue = b;
    }

    public Pixel(String hex) {
        hex = hex.replace("#", "");

        if (hex.length() != 6) {
            throw new IllegalArgumentException("There must be 6 digits here");
        }

        String red = hex.substring(0, 2);
        String green = hex.substring(2, 4);
        String blue = hex.substring(4, 6);

        this.alpha = 0;
        this.red = Integer.parseInt(red,16);
        this.green = Integer.parseInt(green, 16);
        this.blue = Integer.parseInt(blue, 16);
    }

    public Pixel add(Pixel anotherPixel) {
        return new Pixel(
                this.red + anotherPixel.red,
                this.green + anotherPixel.green,
                this.blue + anotherPixel.blue
        );
    }

    public int getAlpha() {
        return alpha;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public double calculateColorDistance(Pixel anotherPixel) {
        return Math.sqrt(
                Math.pow(this.red - anotherPixel.red, 2) +
                        Math.pow(this.green - anotherPixel.green, 2) +
                        Math.pow(this.blue - anotherPixel.blue, 2));
    }

    public double calculateAverageVariationPercentage(Pixel anotherPixel) {
//        double rv = calculateVariationProportion(this.getRed(), anotherPixel.getRed());
//        double gv = calculateVariationProportion(this.getGreen(), anotherPixel.getGreen());
//        double bv = calculateVariationProportion(this.getBlue(), anotherPixel.getBlue());
//
//        return (rv + gv + bv) / 3;
        return calculateBrightnessProportionVariationWithLuma(anotherPixel);
    }

    public double calculateBrightnessProportionVariationWithLuma(Pixel anotherPixel) {
        double thisLuma = calculateLuma();
        double anotherLuma = anotherPixel.calculateLuma();

        return calculateVariationProportion(thisLuma, anotherLuma);
    }

    public double calculateBrightnessDifferenceWithLuma(Pixel anotherPixel) {
        double thisLuma = calculateLuma();
        double anotherLuma = anotherPixel.calculateLuma();

        return -(thisLuma - anotherLuma);
    }

    public double calculateLuma() {
        return 0.299 * this.red + 0.587 * this.green + 0.114 * this.blue;
    }

    private double calculateVariationProportion(double v1, double v2) {
        return (v2 - v1) / (v1);
    }
}
