package mathecat.block.puzzleimagesolver.model;

import java.awt.image.BufferedImage;

public class ImageBlock {
    private Rectangle rectangle;
    private BufferedImage image;

    public ImageBlock(Rectangle rectangle, BufferedImage image) {
        this.rectangle = rectangle;
        this.image = image;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public BufferedImage getImage() {
        return image;
    }
}
