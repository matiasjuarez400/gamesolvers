package mathecat.block.puzzleimagesolver.model;

import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;

public class Range {
    public final double lowerLimit;
    public final double upperLimit;
    public final double center;
    public final double tolerance;

    public Range(double lowerLimit, double upperLimit, double center, double tolerance) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.center = center;
        this.tolerance = tolerance;
    }

    public boolean isInRange(double number) {
        return NumericUtils.isNumberInRange(number, lowerLimit, upperLimit);
    }

    public Range expandToleranceByAmount(double amount) {
        double newTolerance = tolerance + amount;
        return new Range(
                center - newTolerance,
                center + newTolerance,
                center,
                newTolerance
        );
    }

    public Range expandToleranceByProportion(double proportion) {
        double newTolerance = tolerance + tolerance * proportion;
        return new Range(
                center - newTolerance,
                center + newTolerance,
                center,
                newTolerance
        );
    }
}
