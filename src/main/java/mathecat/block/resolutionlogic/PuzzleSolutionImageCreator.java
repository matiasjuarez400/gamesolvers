package mathecat.block.resolutionlogic;

import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.resolutionlogic.model.BlockType;
import mathecat.block.resolutionlogic.model.Board;
import mathecat.block.resolutionlogic.model.PuzzleSolution;
import mathecat.block.resolutionlogic.model.SolutionPiecePosition;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.List;

public class PuzzleSolutionImageCreator {
    private final int BOARD_SQUARE_SIDE_SIZE = 80;
    private final int BOARD_SQUARE_MARGIN = 4;
    private final double PIECE_SIZE_PROPORTION = 0.8;
    private final Color IMAGE_BACKGROUND = Color.BLACK;
    private final Color BOARD_SQUARE_BACKGROUND = Color.LIGHT_GRAY;
    private final int COLOR_ROTATION_STEPS = 3;
    private final int COLOR_ROTATION_STEP_SIZE = 70;
    private ImageIOHandler imageIOHandler = new ImageIOHandler();

    public BufferedImage drawSolution(PuzzleSolution solution) {
        BufferedImage image = generateEmptyImage(solution);
        setImageBackground(image);
        drawBoard(image, solution);
        drawPieces(image, solution);

        imageIOHandler.saveImage(image, "D:\\Documents\\Projects\\problemsolver\\src\\test\\testTempFolder", "solutionInProgress");

        return image;
    }

    private class ColorRotationHandler {
        private int[] currentSteps;
        private int pointer = 0;

        public ColorRotationHandler() {
            this.currentSteps = new int[3];
        }

        public Color getNextColor() {
            currentSteps[pointer] += 1;
            if (currentSteps[pointer] >= COLOR_ROTATION_STEPS) currentSteps[pointer] = 0;

            pointer++;
            if (pointer >= currentSteps.length) pointer = 0;

            Color nextColor = new Color(
                    currentSteps[0] * COLOR_ROTATION_STEP_SIZE,
                    currentSteps[1] * COLOR_ROTATION_STEP_SIZE,
                    currentSteps[2] * COLOR_ROTATION_STEP_SIZE
            );

            return nextColor;
        }
    }

    private void drawPieces(BufferedImage image, PuzzleSolution solution) {
        Board board = solution.getBoard();

        int pieceSize = (int) (BOARD_SQUARE_SIDE_SIZE * PIECE_SIZE_PROPORTION);

        boolean pieceIsEven = pieceSize % 2 == 0;
        boolean boardSquareIsEven = BOARD_SQUARE_SIDE_SIZE % 2 == 0;

        if (pieceIsEven ^ boardSquareIsEven) {
            pieceSize--;
        }

        int padding = (BOARD_SQUARE_SIDE_SIZE - pieceSize) / 2;

        ColorRotationHandler colorRotationHandler = new ColorRotationHandler();
        for (SolutionPiecePosition solutionPiecePosition : solution.getSolutionPiecePositions()) {
            Color nextColor = colorRotationHandler.getNextColor();

            List<BlockType[]> pieceComposition = solutionPiecePosition.getPiece().getComposition();
            for (int y = 0; y < pieceComposition.size(); y++) {
                for (int x = 0; x < pieceComposition.get(y).length; x++) {
                    BlockType blockType = pieceComposition.get(y)[x];

                    if (blockType == BlockType.F) {
                        int ix = calculateBoardSquarePositionByLevel(solutionPiecePosition.getPosition().x + x) + padding;
                        int iy = calculateBoardSquarePositionByLevel(solutionPiecePosition.getPosition().y + y) + padding;

                        fillWithColor(image, ix, iy, pieceSize, pieceSize, nextColor);
                    }
                }
            }
        }
    }

    private void drawBoard(BufferedImage image, PuzzleSolution solution) {
        Board board = solution.getBoard();

        for (int y = 0; y < board.getRows(); y++) {
            for (int x = 0; x < board.getComposition().get(y).length; x++) {
                if (board.getComposition().get(y)[x] != BlockType.E) continue;

                int iy = calculateBoardSquarePositionByLevel(y);
                int ix = calculateBoardSquarePositionByLevel(x);

                fillWithColor(image, ix, iy, BOARD_SQUARE_SIDE_SIZE, BOARD_SQUARE_SIDE_SIZE, BOARD_SQUARE_BACKGROUND);
            }
        }
    }

    private void fillWithColor(BufferedImage image, int ix, int iy, int width, int height, Color color) {
        for (int x = ix; x < ix + width; x++) {
            for (int y = iy; y < iy + height; y++) {
                image.setRGB(x, y, color.getRGB());
            }
        }
    }

    private void setImageBackground(BufferedImage image) {
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                image.setRGB(x, y, IMAGE_BACKGROUND.getRGB());
            }
        }
    }

    private BufferedImage generateEmptyImage(PuzzleSolution solution) {
        int imageHeight = calculateImageDimension(solution.getBoard().getRows());
        int imageWidth = calculateImageDimension(solution.getBoard().getColumns());

        return new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    }

    private int calculateImageDimension(int levels) {
        return levels * (BOARD_SQUARE_SIDE_SIZE + 2 * BOARD_SQUARE_MARGIN) + 2 * BOARD_SQUARE_SIDE_SIZE;
    }

    private int calculateBoardSquarePositionByLevel(int level) {
        return (BOARD_SQUARE_SIDE_SIZE + BOARD_SQUARE_MARGIN) + (BOARD_SQUARE_SIDE_SIZE * level) + (2 * BOARD_SQUARE_MARGIN * level);
    }
}
