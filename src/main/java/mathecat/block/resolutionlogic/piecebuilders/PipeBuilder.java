package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.F;

public class PipeBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        switch (pieceType) {
            case P_2: {
                return buildStandingPipe(2);
            }
            case P_2_LD: {
                return buildLyingDownPipe(2);
            }
            case P_3: {
                return buildStandingPipe(3);
            }
            case P_3_LD: {
                return buildLyingDownPipe(3);
            }
            case P_4: {
                return buildStandingPipe(4);
            }
            case P_4_LD: {
                return buildLyingDownPipe(4);
            }
            default: return null;
        }
    }

    private Piece buildStandingPipe(int size) {
        Piece piece = new Piece();

        for (int i = 0; i < size; i++) {
            piece.addRow(F);
        }

        return piece;
    }

    private Piece buildLyingDownPipe(int size) {
        Piece piece = new Piece();
        piece.addRow(F, size);
        return piece;
    }
}
