package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

public interface PieceBuilder {
    Piece build(PieceType pieceType);
}
