package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class SquareBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = new Piece();

        switch (pieceType) {
            case SQUARE: {
                piece.addRow(F, 2)
                        .addRow(F, 2);
                return piece;
            }
            case SQ_R_D: {
                piece = build(PieceType.SQUARE);
                piece.addRow(E, F);
                return piece;
            }
            case SQ_R_U: {
                piece.addRow(E, F)
                        .addRow(F, 2)
                        .addRow(F, 2);
                return piece;
            }
            case SQ_L_D: {
                piece = build(PieceType.SQUARE);
                piece.addRow(F, E);
                return piece;
            }
            case SQ_L_U: {
                piece.addRow(F, E)
                        .addRow(F, 2)
                        .addRow(F, 2);
                return piece;
            }
            case SQ_R_LD: {
                piece.addRow(F, 2)
                        .addRow(F, 3);
                return piece;
            }
            case SQ_R_LD_I: {
                piece.addRow(F, 3)
                        .addRow(F, 2);
                return piece;
            }
            case SQ_L_LD: {
                piece.addRow(E, F, F)
                        .addRow(F, 3);
                return piece;
            }
            case SQ_L_LD_I: {
                piece.addRow(F, 3)
                        .addRow(E, F, F);
            }
            default: return null;
        }
    }
}
