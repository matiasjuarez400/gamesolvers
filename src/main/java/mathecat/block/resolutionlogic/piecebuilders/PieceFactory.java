package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PieceFactory implements PieceBuilder {
    private List<PieceBuilder> pieceBuilders;

    public PieceFactory() {
        this.pieceBuilders = new ArrayList<>();

        Collections.addAll(pieceBuilders,
                new GenericBlockBuilder(),
                new SquareBuilder(),
                new LBuilder(),
                new PipeBuilder(),
                new SBuilder(),
                new TBuilder());
    }


    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = null;
        for (PieceBuilder pieceBuilder : this.pieceBuilders) {
            piece = pieceBuilder.build(pieceType);
            if (piece != null) {
                break;
            }
        }

        return piece;
    }
}
