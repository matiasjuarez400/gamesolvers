package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class SBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = new Piece();
        switch (pieceType) {
            case S_R: {
                piece.addRow(E, F)
                        .addRow(F, 2)
                        .addRow(F, E);
                break;
            }
            case S_L: {
                piece.addRow(F, E)
                        .addRow(F, 2)
                        .addRow(E, F);
                break;
            }
            case S_R_LD: {
                piece.addRow(E, F, F)
                        .addRow(F, 2);
                break;
            }
            case S_L_LD: {
                piece.addRow(F, 2)
                        .addRow(E, F, F);
                break;
            } default: return null;
        }
        return piece;
    }
}
