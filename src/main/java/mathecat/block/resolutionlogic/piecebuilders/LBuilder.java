package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class LBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = new Piece();
        switch (pieceType) {
            case L_R_D: {
                piece.addRow(F)
                        .addRow(F)
                        .addRow(F, 2);
                break;
            }
            case L_L_D: {
                piece.addRow(E, F)
                        .addRow(E, F)
                        .addRow(F, 2);
                break;
            }
            case L_R_U: {
                piece.addRow(F, 2)
                        .addRow(F)
                        .addRow(F);
                break;
            }
            case L_L_U: {
                piece.addRow(F, 2)
                        .addRow(E, F)
                        .addRow(E, F);
                break;
            }
            case L_R_D_LD: {
                piece.addRow(F)
                        .addRow(F, 3);
                break;
            }
            case L_L_D_LD: {
                piece.addRow(E, E, F)
                        .addRow(F, 3);
                break;
            }
            case L_R_U_LD: {
                piece.addRow(F, 3)
                        .addRow(F);
                break;
            }
            case L_L_U_LD: {
                piece.addRow(F, 3)
                        .addRow(E, E, F);
                break;
            }
            case SL_L_D: {
                piece.addRow(E, F)
                        .addRow(F, 2);
                return piece;
            }
            case SL_L_U: {
                piece.addRow(F, 2)
                        .addRow(E, F);
                return piece;
            }
            case SL_R_D: {
                piece.addRow(F, E)
                        .addRow(F, 2);
                return piece;
            }
            case SL_R_U: {
                piece.addRow(F, 2)
                        .addRow(F, E);
                return piece;
            }
            default: return null;
        }
        return piece;
    }
}
