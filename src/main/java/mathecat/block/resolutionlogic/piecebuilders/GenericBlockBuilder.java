package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class GenericBlockBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = new Piece();

        switch (pieceType) {
            case POINT: {
                piece.addRow(F);
                return piece;
            }
            case CROSS: {
                piece.addRow(E, F, E)
                        .addRow(F, 3)
                        .addRow(E, F, E);
                return piece;
            }
            default: return null;
        }
    }
}
