package mathecat.block.resolutionlogic.piecebuilders;

public enum PieceType {
    L_R_D, L_L_D, L_R_U, L_L_U,
    L_R_D_LD, L_L_D_LD, L_R_U_LD, L_L_U_LD,
    SL_R_U, SL_R_D, SL_L_U, SL_L_D,
    S_R, S_L, S_R_LD, S_L_LD,
    SQUARE, CROSS, POINT,
    P_2, P_2_LD, P_3, P_3_LD, P_4, P_4_LD,
    T_R, T_L, T_U, T_D,
    T2_R, T2_L, T2_U, T2_D,
    SQ_R_U, SQ_L_U, SQ_R_D, SQ_L_D, SQ_R_LD_I, SQ_R_LD, SQ_L_LD_I, SQ_L_LD
}
