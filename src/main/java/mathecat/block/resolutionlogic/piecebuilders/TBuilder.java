package mathecat.block.resolutionlogic.piecebuilders;

import mathecat.block.resolutionlogic.model.Piece;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class TBuilder implements PieceBuilder {
    @Override
    public Piece build(PieceType pieceType) {
        Piece piece = new Piece();

        switch (pieceType) {
            case T_L: {
                piece.addRow(E, F)
                        .addRow(F, 2)
                        .addRow(E, F);
                return piece;
            }
            case T_R: {
                piece.addRow(F, E)
                        .addRow(F, 2)
                        .addRow(F, E);
                return piece;
            }
            case T_D: {
                piece.addRow(F, 3)
                        .addRow(E, F, E);
                return piece;
            }
            case T_U: {
                piece.addRow(E, F, E)
                        .addRow(F, 3);
                return piece;
            }
            case T2_L: {
                piece.addRow(E, E, F)
                        .addRow(F, 3)
                        .addRow(E, E, F);
                return piece;
            }
            case T2_R: {
                piece.addRow(F, E)
                        .addRow(F, 3)
                        .addRow(F, E);
                return piece;
            }
            case T2_D: {
                piece.addRow(F, 3)
                        .addRow(E, F, E)
                        .addRow(E, F, E);
                return piece;
            }
            case T2_U: {
                piece.addRow(E, F, E)
                        .addRow(E, F, E)
                        .addRow(F, 3);
                return piece;
            }
            default: return null;
        }
    }
}
