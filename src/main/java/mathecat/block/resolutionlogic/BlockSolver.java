package mathecat.block.resolutionlogic;

import mathecat.block.resolutionlogic.model.*;

import java.util.ArrayList;
import java.util.List;

public class BlockSolver {
    private PuzzleSolution puzzleSolution;

    public PuzzleSolution solve(Board board, List<Piece> pieces) {
        puzzleSolution = new PuzzleSolution();
        puzzleSolution.setBoard(board);

        Board cloneBoard = board.clone();
        doSolve(cloneBoard, pieces);

        if (puzzleSolution.getSolutionPiecePositions().size() != pieces.size()) {
            throw new IllegalArgumentException("Unable to solve puzzle");
        }

        return puzzleSolution;
    }

    private boolean doSolve(Board solvingBoard, List<Piece> pieces) {
        if (pieces.size() == 0) return true;

        Point upperLeftEmptyBlock = searchUpperLeftEmptyBlock(solvingBoard);
        for (int i = 0; i < pieces.size(); i++) {
            Piece nextPiece = pieces.get(i);
            for (int y = 0; y < nextPiece.getComposition().size(); y++) {
                BlockType[] nextPieceRow = nextPiece.getComposition().get(y);
                for (int x = 0; x < nextPieceRow.length; x++) {
                    if (nextPieceRow[x] == BlockType.E) continue;

                    Point pointToUse = new Point(upperLeftEmptyBlock.x - x, upperLeftEmptyBlock.y - y);

                    if (solvingBoard.canPieceFitInPosition(pointToUse, nextPiece)) {
                        solvingBoard.fillWithPiece(pointToUse, nextPiece);

                        List<Piece> remainingPieces = new ArrayList<>(pieces);
                        remainingPieces.remove(i);

                        if (doSolve(solvingBoard, remainingPieces)) {
                            this.puzzleSolution.getSolutionPiecePositions().add(new SolutionPiecePosition(
                                    pointToUse, nextPiece
                            ));

                            return true;
                        }

                        solvingBoard.removePiece(pointToUse, nextPiece);
                    }
                }
            }

        }
        return false;
    }

    private Point searchUpperLeftEmptyBlock(Board solvingBoard) {
        for (int y = 0; y < solvingBoard.getComposition().size(); y++) {
            BlockType[] nextRow = solvingBoard.getComposition().get(y);

            for (int x = 0; x < nextRow.length; x++) {
                if (nextRow[x] == BlockType.E) {
                    return new Point(x, y);
                }
            }
        }

        throw new IllegalStateException("Unable to find empty block in board");
    }
}
