package mathecat.block.resolutionlogic.model;

import java.util.ArrayList;
import java.util.List;

public class PuzzleSolution {
    private Board board;
    private List<SolutionPiecePosition> solutionPiecePositions;

    public PuzzleSolution() {
        this.solutionPiecePositions = new ArrayList<>();
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<SolutionPiecePosition> getSolutionPiecePositions() {
        return solutionPiecePositions;
    }

    public void setSolutionPiecePositions(List<SolutionPiecePosition> solutionPiecePositions) {
        this.solutionPiecePositions = solutionPiecePositions;
    }

    public String getStringRepresentationOfBoardSolution() {
        List<String[]> boardAsString = new ArrayList<>();
        for (int i = 0; i < this.getBoard().getComposition().size(); i++) {
            BlockType[] boardRow = this.getBoard().getComposition().get(i);
            boardAsString.add(new String[boardRow.length]);
            for (int j = 0; j < boardRow.length; j++) {
                boardAsString.get(i)[j] = boardRow[j].toString();
            }
        }

        for (int i = 0; i < getSolutionPiecePositions().size(); i++) {
            SolutionPiecePosition solutionPiecePosition = getSolutionPiecePositions().get(i);
            Point position = solutionPiecePosition.getPosition();

            for (int y = 0; y < solutionPiecePosition.getPiece().getComposition().size(); y++) {
                BlockType[] pieceRow = solutionPiecePosition.getPiece().getComposition().get(y);
                for (int x = 0; x < pieceRow.length; x++) {
                    if (pieceRow[x] == BlockType.F) {
                        boardAsString.get(position.y + y)[position.x + x] = Integer.toString(i);
                    }
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String[] boardRow : boardAsString) {
            for (String block : boardRow) {
                sb.append(block).append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String separator = "*************************\n";
        sb.append(separator);

        for (int i = 0; i < this.getSolutionPiecePositions().size(); i++) {
            SolutionPiecePosition solutionPiecePosition = this.getSolutionPiecePositions().get(i);

            sb.append(i).append(") Position: ").append(solutionPiecePosition.getPosition()).append("\n");
            sb.append(solutionPiecePosition.getPiece()).append("-------\n");
        }

        sb.append(getStringRepresentationOfBoardSolution());

        sb.append(separator);

        return sb.toString();
    }
}
