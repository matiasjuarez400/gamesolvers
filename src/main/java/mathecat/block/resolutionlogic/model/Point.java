package mathecat.block.resolutionlogic.model;

import java.util.Objects;

public class Point {
    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double calculateDistance(Point point) {
        return Math.sqrt(Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2));
    }

    public Point calculateMiddlePoint(Point anotherPoint) {
        double hdisplasement = this.x < anotherPoint.x ? 1 : -1 * (calculateHorizontalDistance(anotherPoint) / 2.0);
        double vdisplasement = this.y < anotherPoint.y ? 1 : -1 * (calculateVerticalDistance(anotherPoint) / 2.0);

        return new Point(
                (int) (this.x + hdisplasement),
                (int) (this.y + vdisplasement)
        );
    }

    public int calculateVerticalDistance(Point anotherPoint) {
        return Math.abs(this.y - anotherPoint.y);
    }

    public int calculateHorizontalDistance(Point anotherPoint) {
        return Math.abs(this.x - anotherPoint.x);
    }

    public Point moveHorizontally(int value) {
        return new Point(this.x + value, this.y);
    }

    public Point moveVertically(int value) {
        return new Point(this.x, this.y + value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return String.format("X = %s - Y = %s", x, y);
    }
}
