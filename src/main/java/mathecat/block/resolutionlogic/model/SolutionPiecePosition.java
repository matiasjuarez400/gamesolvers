package mathecat.block.resolutionlogic.model;

public class SolutionPiecePosition {
    private Point point;
    private Piece piece;

    public SolutionPiecePosition(Point point, Piece piece) {
        this.point = point;
        this.piece = piece;
    }

    public Point getPosition() {
        return point;
    }

    public Piece getPiece() {
        return piece;
    }

    @Override
    public String toString() {
        return "SolutionPiecePosition{" +
                "point=" + point +
                ", piece=" + piece +
                '}';
    }
}
