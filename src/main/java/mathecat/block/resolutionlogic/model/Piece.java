package mathecat.block.resolutionlogic.model;

import java.util.ArrayList;
import java.util.List;

public class Piece {
    private List<BlockType[]> composition;

    public Piece() {
        this.composition = new ArrayList<>();
    }

    public Piece addRow(BlockType... blockTypes) {
        this.composition.add(blockTypes);
        return this;
    }

    public Piece addRow(BlockType blockType, int howMany) {
        BlockType[] blockTypes = new BlockType[howMany];

        for (int i = 0; i < howMany; i++) {
            blockTypes[i] = blockType;
        }
        this.composition.add(blockTypes);
        return this;
    }

    public List<BlockType[]> getComposition() {
        return composition;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (BlockType[] blockTypes : this.composition) {
            for (BlockType blockType : blockTypes) {
                if (blockType == BlockType.F) {
                    sb.append(blockType.toString());
                } else {
                    sb.append(" ");
                }
                sb.append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
