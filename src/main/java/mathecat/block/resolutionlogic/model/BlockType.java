package mathecat.block.resolutionlogic.model;

public enum BlockType {
    X("blocked"),
    F("filled"),
    E("empty");

    private String value;

    BlockType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
