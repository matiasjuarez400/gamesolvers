package mathecat.block.resolutionlogic.model;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<BlockType[]> composition;
    private List<BlockType> tempRow;

    public Board() {
        this.composition = new ArrayList<>();
    }

    public Board addRow(BlockType... blockTypes) {
        this.composition.add(blockTypes);
        return this;
    }

    public Board addRow(BlockType blockType, int howMany) {
        BlockType[] blocks = new BlockType[howMany];

        for (int i = 0; i < howMany; i++) {
            blocks[i] = blockType;
        }

        return addRow(blocks);
    }

    public Board addRow(int howMany) {
        return addRow(BlockType.E, howMany);
    }

    public Board holdRow(BlockType... blockTypes) {
        return holdRow(blockTypes[0], blockTypes.length);
    }

    public Board holdRow(BlockType blockType, int howMany) {
        if (tempRow == null) {
            tempRow = new ArrayList<>();
        }

        for (int i = 0; i < howMany; i++) {
            this.tempRow.add(blockType);
        }

        return this;
    }

    public Board holdRow(int howMany) {
        return holdRow(BlockType.E, howMany);
    }

    public Board shiftRow(int howMany) {
        return holdRow(BlockType.X, howMany);
    }

    public Board applyRow() {
        if (tempRow == null) {
            throw new IllegalStateException("There is no row in hold at this moment");
        }

        this.addRow(this.tempRow.toArray(new BlockType[this.tempRow.size()]));

        this.tempRow = null;

        return this;
    }

    public List<BlockType[]> getComposition() {
        return composition;
    }

    public void fillWithPiece(Point point, Piece piece) {
        fillPieceFigure(point, piece, BlockType.F);
    }

    public void removePiece(Point point, Piece piece) {
        fillPieceFigure(point, piece, BlockType.E);
    }

    private void fillPieceFigure(Point point, Piece piece, BlockType blockType) {
        for (int y = 0; y < piece.getComposition().size(); y++) {
            BlockType[] pieceRow = piece.getComposition().get(y);
            for (int x = 0; x < pieceRow.length; x++) {
                if (pieceRow[x] == BlockType.F) {
                    getComposition().get(point.y + y)[point.x + x] = blockType;
                }
            }
        }
    }

    public boolean canPieceFitInPosition(Point point, Piece piece) {
        for (int y = 0; y < piece.getComposition().size(); y++) {
            BlockType[] pieceRow = piece.getComposition().get(y);
            for (int x = 0; x < pieceRow.length; x++) {
                if (pieceRow[x] == BlockType.F) {
                    int xToAnalyze = point.x + x;
                    int yToAnalyze = point.y + y;

                    if (xToAnalyze < 0 || yToAnalyze < 0 ||
                            yToAnalyze >= getComposition().size() || xToAnalyze >= getComposition().get(yToAnalyze).length) {
                        return false;
                    }

                    BlockType solvingBoardBlock = getComposition().get(yToAnalyze)[xToAnalyze];
                    if (solvingBoardBlock != BlockType.E) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (BlockType[] blockTypes : this.composition) {
            for (BlockType blockType : blockTypes) {
                sb.append(blockType.toString()).append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public Board clone() {
        Board board = new Board();

        for (BlockType[] blockTypes : this.composition) {
            BlockType[] clonedBlocks = new BlockType[blockTypes.length];
            for (int i = 0; i < blockTypes.length; i++) {
                clonedBlocks[i] = blockTypes[i];
            }
            board.addRow(clonedBlocks);
        }

        return board;
    }

    public int getRows() {
        return getComposition().size();
    }

    public int getColumns() {
        int columns = 0;

        for (BlockType[] row : getComposition()) {
            if (row.length > columns) columns = row.length;
        }

        return columns;
    }
}
