package mathecat.block.resolutionlogic.model;

import java.util.List;

public class Puzzle {
    private Board board;
    private List<Piece> pieces;

    public Puzzle(Board board, List<Piece> pieces) {
        this.board = board;
        this.pieces = pieces;
    }

    public Board getBoard() {
        return board;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    @Override
    public String toString() {
        String separator = "***************************\n";
        StringBuilder sb = new StringBuilder(separator);
        sb.append(board.toString()).append("\n");

        for (Piece piece : pieces) {
            sb.append(piece.toString()).append("\n");
        }
        sb.append(separator);

        return sb.toString();
    }
}
