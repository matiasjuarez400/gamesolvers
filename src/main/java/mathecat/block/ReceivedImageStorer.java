package mathecat.block;

import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReceivedImageStorer {
    private String folderPath = "D:\\Documents\\Projects\\problemsolver\\src\\main\\resources\\receivedimages";
    private File storeFolder = new File(folderPath);
    private static final double WIDTH_TO_ANALYSE_PROP = 0.20;
    private static final double HEIGHT_TO_ANALYSE_PROP = 0.20;
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    public void storeIfUnique(BufferedImage receivedImage) {
        int widthToAnalyse = (int) (receivedImage.getWidth() * WIDTH_TO_ANALYSE_PROP);
        int heightToAnalyse = (int) (receivedImage.getHeight() * HEIGHT_TO_ANALYSE_PROP);

        for (File storedImageFile : getAllFilesFromFolder()) {
            BufferedImage storedImage = imageIOHandler.loadImageFromFile(storedImageFile);

            boolean areSameImage = true;
            ROW_FOR: for (int x = 0; x < widthToAnalyse; x++) {
                for (int y = 0; y < heightToAnalyse; y++) {
                    if (receivedImage.getRGB(x, y) != storedImage.getRGB(x, y)) {
                        areSameImage = false;
                        break ROW_FOR;
                    }
                }
            }

            if (areSameImage) return;
        }

        imageIOHandler.saveImage(receivedImage, folderPath, sdf.format(new Date()));
    }

    private File[] getAllFilesFromFolder() {
        return storeFolder.listFiles();
    }
}
