package mathecat.controllers.block;

import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipelineResult;
import mathecat.block.resolutionlogic.model.PuzzleSolution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlocksController {
    private BlocksService service;

    @Autowired
    public BlocksController(BlocksService service) {
        this.service = service;
    }

    @RequestMapping(path = "/solvehtml", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView solvePuzzleHtml(@RequestParam("image") MultipartFile image) {
        ProcessPipelineResult result = service.processUploadedImage(image);

        ModelAndView modelAndView = new ModelAndView();

        if (result.getPuzzleSolution() != null) {
            modelAndView.addObject("textSolution", result.getPuzzleSolution().toString());
        }
        modelAndView.addObject("imageSolution", result.getSolutionImageBase64());

        if (result.getBoardSectionPostProcessingResult() != null) {
            modelAndView.addObject("detectedBoard", result.getBoardSectionPostProcessingResult().getDetectedBoardBase64());
        }

        modelAndView.setViewName("index");

        return modelAndView;
    }

    @RequestMapping(path = "/solve", method = {RequestMethod.GET, RequestMethod.POST})
    @CrossOrigin(origins = "*")
    @ResponseBody
    public BlockSolveResponse solvePuzzle(@RequestParam("image") MultipartFile image) {
        ProcessPipelineResult result = service.processUploadedImage(image);

        BlockSolveResponse response = new BlockSolveResponse();
        response.setImageSolution(result.getSolutionImageBase64());

        if (result.getBoardSectionPostProcessingResult() != null) {
            response.addMoreImage(result.getBoardSectionPostProcessingResult().getDetectedBoardBase64());
        }

        return response;
    }

    @RequestMapping(path = "/index")
    @ResponseBody
    public ModelAndView index() {
        return new ModelAndView("index");
    }
}
