package mathecat.controllers.block;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BlockSolveResponse {
    private String imageSolution;
    private List<String> moreImages;

    public BlockSolveResponse() {
        this.moreImages = new ArrayList<>();
    }

    public void addMoreImage(String anotherImage) {
        this.moreImages.add(anotherImage);
    }
}
