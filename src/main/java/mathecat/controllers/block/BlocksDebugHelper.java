package mathecat.controllers.block;

import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipelineResult;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.resolutionlogic.model.Point;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlocksDebugHelper {
    private String debugFolder = "D:\\Documents\\Projects\\problemsolver\\src\\main\\resources\\debug\\";
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private ImageModifier imageModifier = new ImageModifier();
    private boolean runInDebugMode = true;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    public void printImages(ProcessPipelineResult result) {
        if (!runInDebugMode) return;

        String subfolder = sdf.format(new Date());

        File filesFolder = new File(debugFolder, subfolder);
        filesFolder.mkdirs();

        printBoard(result, filesFolder);

        System.out.println(result.getPuzzleSolution());
    }

    private void printBoard(ProcessPipelineResult result, File folder) {
        BufferedImage boardImage = result.getPuzzleRegionsResult().getBoardSectionImage();

        BoardSectionResult boardSectionResult = result.getBoardSectionResult();
        List<Point> boardPoints = new ArrayList<>();
        boardPoints.addAll(boardSectionResult.getDownPoints());
        boardPoints.addAll(boardSectionResult.getUpPoints());
        boardPoints.addAll(boardSectionResult.getLeftPoints());
        boardPoints.addAll(boardSectionResult.getRightPoints());

        imageIOHandler.saveImage(imageModifier.drawPointsInImage(boardImage,
                boardPoints), folder.getAbsolutePath(), "boardAllPoints");
        imageIOHandler.saveImage(imageModifier.drawPointsInImage(boardImage,
                boardSectionResult.getDownPoints()), folder.getAbsolutePath(), "boardDownPoints");
        imageIOHandler.saveImage(imageModifier.drawPointsInImage(boardImage,
                boardSectionResult.getUpPoints()), folder.getAbsolutePath(), "boardUpPoints");
        imageIOHandler.saveImage(imageModifier.drawPointsInImage(boardImage,
                boardSectionResult.getLeftPoints()), folder.getAbsolutePath(), "boardLeftPoints");
        imageIOHandler.saveImage(imageModifier.drawPointsInImage(boardImage,
                boardSectionResult.getRightPoints()), folder.getAbsolutePath(), "boardRightPoints");
    }
}
