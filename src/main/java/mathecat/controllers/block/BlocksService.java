package mathecat.controllers.block;

import mathecat.block.ReceivedImageStorer;
import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipeline;
import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipelineResult;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Component
public class BlocksService {
    private ProcessPipeline processPipeline;
    private BlocksDebugHelper blocksDebugHelper;
    private ReceivedImageStorer receivedImageStorer;

    public BlocksService() {
        this.processPipeline = new ProcessPipeline();
        this.blocksDebugHelper = new BlocksDebugHelper();
        this.receivedImageStorer = new ReceivedImageStorer();
    }

    public ProcessPipelineResult processUploadedImage(MultipartFile multipartFile) {
        try {
            BufferedImage bufferedImage = ImageIO.read(multipartFile.getInputStream());
            ProcessPipelineResult result = processPipeline.analyse(bufferedImage);

            blocksDebugHelper.printImages(result);

            receivedImageStorer.storeIfUnique(bufferedImage);

            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
