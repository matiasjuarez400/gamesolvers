package mathecat.block;

import junit.framework.TestCase;
import mathecat.block.resolutionlogic.BlockSolver;
import mathecat.block.resolutionlogic.piecebuilders.PieceFactory;
import mathecat.block.resolutionlogic.piecebuilders.PieceType;
import mathecat.block.resolutionlogic.model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static mathecat.block.resolutionlogic.model.BlockType.E;
import static mathecat.block.resolutionlogic.model.BlockType.F;

public class BlockSolverTest extends TestCase {
    private static PieceFactory pieceFactory = new PieceFactory();

    @Test
    public void testSolvePuzzle651() {
        Board board = new Board()
                .shiftRow(3).holdRow(4).applyRow()
                .shiftRow(2).holdRow(2).holdRow(BlockType.X, 1).holdRow(2).applyRow()
                .shiftRow(1).holdRow(2).holdRow(BlockType.X, 2).holdRow(2).applyRow()
                .holdRow(2).holdRow(BlockType.X, 2).holdRow(3).applyRow()
                .holdRow(1).holdRow(BlockType.X, 2).holdRow(3).applyRow()
                .holdRow(2).holdRow(BlockType.X, 1).holdRow(3).applyRow()
                .addRow(6);

        List<Piece> pieces = new ArrayList<>();
        Collections.addAll(pieces,
                pieceFactory.build(PieceType.L_L_D_LD),
                pieceFactory.build(PieceType.P_2),
                pieceFactory.build(PieceType.SQ_R_LD_I),
                pieceFactory.build(PieceType.L_R_D),
                pieceFactory.build(PieceType.P_3_LD),
                pieceFactory.build(PieceType.SL_R_U),
                pieceFactory.build(PieceType.SQ_R_LD),
                pieceFactory.build(PieceType.S_R),
                pieceFactory.build(PieceType.P_2_LD)
        );

        Puzzle puzzle = new Puzzle(board, pieces);

        BlockSolver blockSolver = new BlockSolver();
        PuzzleSolution puzzleSolution = blockSolver.solve(puzzle.getBoard(), puzzle.getPieces());

        System.out.println(puzzleSolution);

        assertEquals(9, puzzleSolution.getSolutionPiecePositions().size());
        assertEquals(new Point(3, 5), puzzleSolution.getSolutionPiecePositions().get(0).getPosition());
        assertEquals(new Point(0, 5), puzzleSolution.getSolutionPiecePositions().get(1).getPosition());
        assertEquals(new Point(3, 4), puzzleSolution.getSolutionPiecePositions().get(2).getPosition());
        assertEquals(new Point(4, 3), puzzleSolution.getSolutionPiecePositions().get(3).getPosition());
        assertEquals(new Point(0, 2), puzzleSolution.getSolutionPiecePositions().get(4).getPosition());
        assertEquals(new Point(2, 1), puzzleSolution.getSolutionPiecePositions().get(5).getPosition());
        assertEquals(new Point(6, 0), puzzleSolution.getSolutionPiecePositions().get(6).getPosition());
        assertEquals(new Point(5, 0), puzzleSolution.getSolutionPiecePositions().get(7).getPosition());
        assertEquals(new Point(3, 0), puzzleSolution.getSolutionPiecePositions().get(8).getPosition());
    }

    @Test
    public void testSolvePuzzle652() {
        Board board = new Board()
                .addRow(7)
                .holdRow(3).holdRow(BlockType.X, 2).holdRow(2).applyRow()
                .addRow(7)
                .addRow(7)
                .shiftRow(1).holdRow(5).applyRow()
                .shiftRow(1).holdRow(3).applyRow();

        List<Piece> pieces = new ArrayList<>();
        Collections.addAll(pieces,
                pieceFactory.build(PieceType.SL_L_U),
                pieceFactory.build(PieceType.SL_R_D),
                pieceFactory.build(PieceType.SQ_R_LD),
                pieceFactory.build(PieceType.SQUARE),
                pieceFactory.build(PieceType.T2_D),
                pieceFactory.build(PieceType.SL_L_D),
                pieceFactory.build(PieceType.P_3_LD),
                pieceFactory.build(PieceType.L_R_U),
                pieceFactory.build(PieceType.S_R_LD)
        );

        Puzzle puzzle = new Puzzle(board, pieces);

        BlockSolver blockSolver = new BlockSolver();
        PuzzleSolution puzzleSolution = blockSolver.solve(puzzle.getBoard(), puzzle.getPieces());

        System.out.println(puzzleSolution);

        List<SolutionPiecePosition> solutionPiecePositions = puzzleSolution.getSolutionPiecePositions();
        assertEquals(9, solutionPiecePositions.size());
        assertEquals(new Point(2, 4), solutionPiecePositions.get(0).getPosition());
        assertEquals(new Point(4, 3), solutionPiecePositions.get(1).getPosition());
        assertEquals(new Point(0, 3), solutionPiecePositions.get(2).getPosition());
        assertEquals(new Point(3, 2), solutionPiecePositions.get(3).getPosition());
        assertEquals(new Point(5, 1), solutionPiecePositions.get(4).getPosition());
        assertEquals(new Point(1, 1), solutionPiecePositions.get(5).getPosition());
        assertEquals(new Point(5, 0), solutionPiecePositions.get(6).getPosition());
        assertEquals(new Point(2, 0), solutionPiecePositions.get(7).getPosition());
        assertEquals(new Point(0, 0), solutionPiecePositions.get(8).getPosition());
    }

    @Test
    public void testSolvePuzzle653() {
        Board board = new Board()
                .shiftRow(2).holdRow(1).shiftRow(1).holdRow(1).applyRow()
                .shiftRow(1).holdRow(6).applyRow()
                .addRow(7)
                .addRow(7)
                .addRow(7)
                .shiftRow(3).holdRow(2).applyRow();

        List<Piece> pieces = new ArrayList<>();
        Collections.addAll(pieces,
                pieceFactory.build(PieceType.P_2),
                pieceFactory.build(PieceType.POINT),
                new Piece().addRow(E, F, E)
                        .addRow(F, 3)
                        .addRow(E, E, F),
                pieceFactory.build(PieceType.SQUARE),
                pieceFactory.build(PieceType.L_R_U_LD),
                pieceFactory.build(PieceType.P_3),
                pieceFactory.build(PieceType.S_R),
                pieceFactory.build(PieceType.T_R),
                pieceFactory.build(PieceType.T_R)
        );

        Puzzle puzzle = new Puzzle(board, pieces);

        BlockSolver blockSolver = new BlockSolver();
        PuzzleSolution puzzleSolution = blockSolver.solve(puzzle.getBoard(), puzzle.getPieces());

        System.out.println(puzzleSolution);

        List<SolutionPiecePosition> solutionPiecePositions = puzzleSolution.getSolutionPiecePositions();

        assertEquals(9, solutionPiecePositions.size());
        assertEquals(new Point(0, 4), solutionPiecePositions.get(0).getPosition());
        assertEquals(new Point(6, 3), solutionPiecePositions.get(1).getPosition());
        assertEquals(new Point(4, 3), solutionPiecePositions.get(2).getPosition());
        assertEquals(new Point(3, 3), solutionPiecePositions.get(3).getPosition());
        assertEquals(new Point(1, 3), solutionPiecePositions.get(4).getPosition());
        assertEquals(new Point(0, 2), solutionPiecePositions.get(5).getPosition());
        assertEquals(new Point(5, 1), solutionPiecePositions.get(6).getPosition());
        assertEquals(new Point(4, 0), solutionPiecePositions.get(7).getPosition());
        assertEquals(new Point(1, 0), solutionPiecePositions.get(8).getPosition());
    }

    @Test
    public void testSolvePuzzle654() {
        Board board = new Board()
                .shiftRow(1).holdRow(4).applyRow()
                .addRow(5)
                .holdRow(2).holdRow(BlockType.X, 2).holdRow(1).applyRow()
                .holdRow(2).holdRow(BlockType.X, 1).holdRow(2).applyRow()
                .shiftRow(1).holdRow(4).applyRow()
                .addRow(5)
                .shiftRow(1).holdRow(4).applyRow();

        List<Piece> pieces = new ArrayList<>();
        Collections.addAll(pieces,
                pieceFactory.build(PieceType.P_3_LD),
                pieceFactory.build(PieceType.T_L),
                new Piece().addRow(E, E, F, E)
                        .addRow(F, 4),
                pieceFactory.build(PieceType.SQ_R_D),
                pieceFactory.build(PieceType.T_L),
                pieceFactory.build(PieceType.L_L_D_LD),
                pieceFactory.build(PieceType.L_R_U_LD)
        );

        Puzzle puzzle = new Puzzle(board, pieces);

        BlockSolver blockSolver = new BlockSolver();
        PuzzleSolution puzzleSolution = blockSolver.solve(puzzle.getBoard(), puzzle.getPieces());

        System.out.println(puzzleSolution);

        List<SolutionPiecePosition> solutionPiecePositions = puzzleSolution.getSolutionPiecePositions();

        assertEquals(7, solutionPiecePositions.size());
        assertEquals(new Point(1, 5), solutionPiecePositions.get(0).getPosition());
        assertEquals(new Point(0, 4), solutionPiecePositions.get(1).getPosition());
        assertEquals(new Point(3, 3), solutionPiecePositions.get(2).getPosition());
        assertEquals(new Point(0, 2), solutionPiecePositions.get(3).getPosition());
        assertEquals(new Point(0, 1), solutionPiecePositions.get(4).getPosition());
        assertEquals(new Point(3, 0), solutionPiecePositions.get(5).getPosition());
        assertEquals(new Point(1, 0), solutionPiecePositions.get(6).getPosition());
    }
}
