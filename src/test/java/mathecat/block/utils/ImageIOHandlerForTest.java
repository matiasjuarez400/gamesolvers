package mathecat.block.utils;

import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;

import java.awt.image.BufferedImage;
import java.io.File;

public class ImageIOHandlerForTest extends ImageIOHandler {
    public void saveImage(BufferedImage bufferedImage, Class caller, String imageName) {
        String folder = TestingValues.TEST_TEMP_FOLDER + File.separator + caller.getSimpleName();

        saveImage(bufferedImage, folder, imageName);
    }
}
