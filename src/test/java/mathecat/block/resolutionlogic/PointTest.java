package mathecat.block.resolutionlogic;

import junit.framework.TestCase;
import mathecat.block.resolutionlogic.model.Point;
import org.junit.Test;

public class PointTest extends TestCase {
    @Test
    public void testCalculateDistance() {
        double delta = 0.001;

        Point p1 = new Point(10, 5);
        Point p2 = new Point(12, 5);

        assertEquals(2.0, p1.calculateDistance(p2), delta);
        assertEquals(2.0, p2.calculateDistance(p1), delta);

        p2 = new Point(10, 4);
        assertEquals(1.0, p1.calculateDistance(p2), delta);

        p2 = new Point(15, 2);
        assertEquals(5.8309, p1.calculateDistance(p2), delta);
        assertEquals(5.8309, p2.calculateDistance(p1), delta);

        assertEquals(0, p1.calculateDistance(p1), delta);
    }

    @Test
    public void testCalculateVerticalDistance() {
        Point p1 = new Point(10, 5);
        Point p2 = new Point(12, 2);

        assertEquals(3, p1.calculateVerticalDistance(p2));
        assertEquals(3, p2.calculateVerticalDistance(p1));
        assertEquals(0, p2.calculateVerticalDistance(p2));
    }

    @Test
    public void testCalculateHorizontalDistance() {
        Point p1 = new Point(10, 5);
        Point p2 = new Point(12, 2);

        assertEquals(2, p1.calculateHorizontalDistance(p2));
        assertEquals(2, p2.calculateHorizontalDistance(p1));
        assertEquals(0, p1.calculateHorizontalDistance(p1));
    }

    @Test
    public void testCalculateMiddlePoint() {
        Point p1 = new Point(10, 5);
        Point p2 = new Point(12, 2);

        Point middlePoint1 = p1.calculateMiddlePoint(p2);

        assertEquals(11, middlePoint1.x);
        assertEquals(3, middlePoint1.y);

        Point middlePoint2 = p2.calculateMiddlePoint(p1);

        assertEquals(11, middlePoint2.x);
        assertEquals(3, middlePoint2.y);
    }
}
