package mathecat.block.resolutionlogic;

import junit.framework.TestCase;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.model.Rectangle;
import org.junit.Test;

import java.util.List;

public class RectangleTest extends TestCase {

    @Test
    public void testGetVertex() {
        Rectangle rectangle = new Rectangle(new Point(2, 4), new Point(3, 7));

        Point upperLeft = rectangle.getUpperLeftVertex();

        assertEquals(2, upperLeft.x);
        assertEquals(4, upperLeft.y);

        Point upperRight = rectangle.getUpperRightVertex();
        assertEquals(3, upperRight.x);
        assertEquals(4, upperRight.y);

        Point lowerLeft = rectangle.getLowerLeftVertex();
        assertEquals(2, lowerLeft.x);
        assertEquals(7, lowerLeft.y);

        Point lowerRight = rectangle.getLowerRightVertex();
        assertEquals(3, lowerRight.x);
        assertEquals(7, lowerRight.y);

        List<Point> allVertex = rectangle.getVertex();
        assertTrue(allVertex.contains(upperLeft));
        assertTrue(allVertex.contains(upperRight));
        assertTrue(allVertex.contains(lowerLeft));
        assertTrue(allVertex.contains(lowerRight));

        assertFalse(allVertex.contains(new Point(upperLeft.x, upperLeft.y + 20)));
    }

    @Test
    public void testGeometryFunctions() {
        Rectangle rectangle = new Rectangle(new Point(1, 4), new Point(5, 7));

        assertEquals(4, rectangle.getWidth());
        assertEquals(3, rectangle.getHeight());

        assertEquals(12, rectangle.getArea());
    }

    @Test
    public void testContainsPoint() {
        Rectangle rectangle = new Rectangle(new Point(1, 4), new Point(5, 7));

        assertTrue(rectangle.containsPoint(new Point(1, 4)));
        assertTrue(rectangle.containsPoint(new Point(1, 7)));
        assertTrue(rectangle.containsPoint(new Point(1, 5)));
        assertTrue(rectangle.containsPoint(new Point(3, 4)));
        assertTrue(rectangle.containsPoint(new Point(2, 5)));

        assertFalse(rectangle.containsPoint(new Point(4, 1)));
        assertFalse(rectangle.containsPoint(new Point(0, 4)));
        assertFalse(rectangle.containsPoint(new Point(6, 4)));
        assertFalse(rectangle.containsPoint(new Point(2, 8)));
        assertFalse(rectangle.containsPoint(new Point(2, 3)));
    }

    @Test
    public void testOverlaping() {
        Rectangle r1 = new Rectangle(new Point(1, 4), new Point(5, 7));

        assertFalse(r1.overlapsWith(new Rectangle(new Point(6, 4), new Point(7, 7))));
        assertFalse(r1.overlapsWith(new Rectangle(new Point(-1, 4), new Point(0, 7))));
        assertFalse(r1.overlapsWith(new Rectangle(new Point(1, 8), new Point(5, 9))));
        assertFalse(r1.overlapsWith(new Rectangle(new Point(1, 2), new Point(7, 3))));

        assertTrue(r1.overlapsWith(new Rectangle(new Point(1, 4), new Point(5, 7))));
        assertTrue(r1.overlapsWith(new Rectangle(new Point(0, 5), new Point(6, 6))));
        assertTrue(r1.overlapsWith(new Rectangle(new Point(0, 2), new Point(6, 8))));
    }

    @Test
    public void testWrongRectangleInitialization() {
        try {
            Rectangle r1 = new Rectangle(new Point(2, 6), new Point(1, 7));
            fail("Expected exception");
        } catch (RuntimeException re) {
            assertEquals("Point p2 has to be right-down relative to point p1", re.getMessage());
        }

        try {
            Rectangle r1 = new Rectangle(new Point(2, 6), new Point(2, 7));
            fail("Expected exception");
        } catch (RuntimeException re) {
            assertEquals("Point p2 has to be right-down relative to point p1", re.getMessage());
        }

        try {
            Rectangle r1 = new Rectangle(new Point(2, 6), new Point(3, 5));
            fail("Expected exception");
        } catch (RuntimeException re) {
            assertEquals("Point p2 has to be right-down relative to point p1", re.getMessage());
        }

        try {
            Rectangle r1 = new Rectangle(new Point(2, 6), new Point(3, 6));
            fail("Expected exception");
        } catch (RuntimeException re) {
            assertEquals("Point p2 has to be right-down relative to point p1", re.getMessage());
        }

        try {
            Rectangle r1 = new Rectangle(new Point(2, 6), new Point(1, 6));
            fail("Expected exception");
        } catch (RuntimeException re) {
            assertEquals("Point p2 has to be right-down relative to point p1", re.getMessage());
        }
    }

    @Test
    public void testWidthHeightRatio() {
        Rectangle r1 = new Rectangle(new Point(1, 4), new Point(3, 9));

        assertEquals(0.4, r1.getWidthHeightRatio());

        Rectangle r2 = new Rectangle(new Point(4, 1), new Point(9, 3));
        assertEquals(2.5, r2.getWidthHeightRatio());

        Rectangle r3 = new Rectangle(new Point(4, 5), new Point(8, 9));
        assertEquals(1.0, r3.getWidthHeightRatio());
    }

    @Test
    public void testCalculateClosestVertexDistance() {
        Rectangle r1 = new Rectangle(new Point(1, 4), new Point(5, 9));

        assertEquals(1.0, r1.calculateClosestVertexDistance(new Rectangle(new Point(6, 4), new Point(7, 9))));
        assertEquals(1.0, r1.calculateClosestVertexDistance(new Rectangle(new Point(-2, 4), new Point(0, 9))));
        assertEquals(1.0, r1.calculateClosestVertexDistance(new Rectangle(new Point(1, 2), new Point(3, 3))));
        assertEquals(2.0, r1.calculateClosestVertexDistance(new Rectangle(new Point(1, 6), new Point(3, 7))));
        assertEquals(2.8284, r1.calculateClosestVertexDistance(new Rectangle(new Point(-2, -6), new Point(3, 7))), 0.001);
    }
}
