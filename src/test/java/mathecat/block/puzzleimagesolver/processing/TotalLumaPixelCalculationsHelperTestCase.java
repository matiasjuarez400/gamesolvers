package mathecat.block.puzzleimagesolver.processing;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;
import org.junit.Before;
import org.junit.Test;

public class TotalLumaPixelCalculationsHelperTestCase extends TestCase {
    private TotalLumaPixelCalculationsHelper totalLumaPixelCalculationsHelper;

    @Before
    public void setUp() {
        totalLumaPixelCalculationsHelper = TotalLumaPixelCalculationsHelper.getInstance();
    }

    @Test
    public void testCalculateBrightnessVariation() {
        Pixel pixel1 = new Pixel(26, 87, 145);
        Pixel pixel2 = new Pixel(7, 45, 81);

        Pixel pixel3 = new Pixel(22, 84, 143);
        Pixel pixel4 = new Pixel(10, 44, 81);

        Pixel pixel5 = new Pixel(26, 83, 147);
        Pixel pixel6 = new Pixel(7, 45, 81);

        Pixel pixel7 = new Pixel(255, 255, 255);
        Pixel pixel8 = new Pixel(0, 0, 0);

        double delta = 0.0001;

        assertEquals(-0.33267, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel1, pixel2), delta);
        assertEquals(-0.30965, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel3, pixel4), delta);
        assertEquals(-0.31993, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel5, pixel6), delta);
        assertEquals(-1.0, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel7, pixel8), delta);

        assertEquals(0.33267, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel2, pixel1), delta);
        assertEquals(0.30965, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel4, pixel3), delta);
        assertEquals(0.31993, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel6, pixel5), delta);
        assertEquals(1.0, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel8, pixel7), delta);

        assertEquals(0, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel8, pixel8), delta);
        assertEquals(0, totalLumaPixelCalculationsHelper.calculateBrightnessVariation(pixel6, pixel6), delta);
    }
}
