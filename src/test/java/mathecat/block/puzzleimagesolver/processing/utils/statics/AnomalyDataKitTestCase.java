package mathecat.block.puzzleimagesolver.processing.utils.statics;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.AnomalyDataKit;
import org.junit.Test;

public class AnomalyDataKitTestCase extends TestCase {
    @Test
    public void testCreateAnomalyDataKit() {
        double[] evenValues = {
                71, 70, 73, 70,
                70, 69, 70, 72,
                71, 300, 71, 69
        };

        AnomalyDataKit anomalyDataKit = AnomalyDataKit.create(evenValues);

        double delta = 0.0001;

        assertEquals(67.75, anomalyDataKit.getLowerInnerLimit(), delta);
        assertEquals(73.75, anomalyDataKit.getUpperInnerLimit(), delta);

        assertEquals(65.5, anomalyDataKit.getLowerOuterLimit(), delta);
        assertEquals(76, anomalyDataKit.getUpperOuterLimit(), delta);

        assertEquals(70, anomalyDataKit.getQ1(), delta);
        assertEquals(70.5, anomalyDataKit.getQ2(), delta);
        assertEquals(71.5, anomalyDataKit.getQ3(), delta);
    }
}
