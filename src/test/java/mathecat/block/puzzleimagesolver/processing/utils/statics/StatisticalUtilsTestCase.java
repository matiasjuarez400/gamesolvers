package mathecat.block.puzzleimagesolver.processing.utils.statics;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.StatisticalUtils;
import org.junit.Test;

public class StatisticalUtilsTestCase extends TestCase {
    @Test
    public void testCalculateMedian() {
        double[] evenValues = {
                71, 70, 73, 70,
                70, 69, 70, 72,
                71, 300, 71, 69
        };

        assertEquals(70.5, StatisticalUtils.median(evenValues), 0.0001);

        double[] oddValues = {
                71, 70, 73, 70,
                70, 69, 65, 70, 72,
                71, 300, 71, 69
        };

        assertEquals(70, StatisticalUtils.median(oddValues), 0.0001);
    }

    @Test
    public void testCalculateAvg() {
        double[] values = {9, -1, 3, 45};

        assertEquals(14.0, StatisticalUtils.average(values));
    }

    @Test
    public void testCalculateStandardDeviation() {
        double[] values = {5, 6, 6, 7, 8};

        assertEquals(1.0198, StatisticalUtils.standardDeviation(values), 0.0001);
    }
}
