package mathecat.block.puzzleimagesolver.processing.utils;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.utils.statistics.AnomalyDataKit;
import mathecat.block.resolutionlogic.model.Point;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class NumericUtilsTestCase extends TestCase {
    @Test
    public void testIsNumberInRange() {
        double lowerLimit = 2.3;
        double upperLimit = 5.8;

        assertTrue(NumericUtils.isNumberInRange(2.3, lowerLimit, upperLimit));
        assertTrue(NumericUtils.isNumberInRange(3.5, lowerLimit, upperLimit));
        assertTrue(NumericUtils.isNumberInRange(5.8, lowerLimit, upperLimit));

        assertFalse(NumericUtils.isNumberInRange(2.29999, lowerLimit, upperLimit));
        assertFalse(NumericUtils.isNumberInRange(-2.29999, lowerLimit, upperLimit));
        assertFalse(NumericUtils.isNumberInRange(5.800001, lowerLimit, upperLimit));
    }

    @Test
    public void testIsNumberAroundValue() {
        double middleValue = 4.05;
        double middleDistance = 1.75;

        assertTrue(NumericUtils.isNumberAroundValue(2.3, middleValue, middleDistance));
        assertTrue(NumericUtils.isNumberAroundValue(3.5, middleValue, middleDistance));
        assertTrue(NumericUtils.isNumberAroundValue(5.8, middleValue, middleDistance));

        assertFalse(NumericUtils.isNumberAroundValue(2.29999, middleValue, middleDistance));
        assertFalse(NumericUtils.isNumberAroundValue(-2.29999, middleValue, middleDistance));
        assertFalse(NumericUtils.isNumberAroundValue(5.800001, middleValue, middleDistance));
    }

    @Test
    public void testCalculateRangeAvg_PointList() {
        List<Point> points = Arrays.asList(
                new Point(8, 6),
                new Point(3, 9),
                new Point(10, 5)
        );

        Point center = NumericUtils.calculateRangeAvg(points);

        assertEquals(7, center.x);
        assertEquals(6, center.y);
    }

    @Test
    public void testCalculateRangeCenter_PointList() {
        List<Point> points = Arrays.asList(
            new Point(8, 6),
            new Point(3, 9),
            new Point(10, 5)
        );

        Point center = NumericUtils.calculateRangeCenter(points);

        assertEquals(6, center.x);
        assertEquals(7, center.y);
    }

    @Test
    public void testCalculateMin() {
        double[] values = {9, -1, 3, 45};

        assertEquals(-1.0, NumericUtils.calculateMin(values));
    }

    @Test
    public void testCalculateMax() {
        double[] values = {9, -1, 3, 45};

        assertEquals(45.0, NumericUtils.calculateMax(values));
    }

    @Test
    public void testCalculateMaxDistanceFromAvg() {
        double[] values = {9, -1, 3, 45};

        assertEquals(31.0, NumericUtils.calculateMaxDistanceFromAvg(values));
    }

    @Test
    public void testCalculateRangeCenter() {
        double[] values = {9, -1, 3, 45};

        assertEquals(22.0, NumericUtils.calculateRangeCenter(values));
    }

    @Test
    public void testCalculateMaxDistanceToRangeCenter() {
        double[] values = {9, -1, 3, 45};

        assertEquals(23.0, NumericUtils.calculateMaxDistanceToRangeCenter(values));
    }
}
