package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.BoardSquareSampleResult;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;
import org.junit.Test;

public class BoardSquareSampleAnalyserTestCase extends TestCase {
    private BoardSquareSampleAnalyser analyser = new BoardSquareSampleAnalyser();

    @Test
    public void testAnalyse() {
        BoardSquareSampleResult result = analyser.analyse();

        assertTrue(result.getLeftLimit().center < result.getRightLimit().center);
        assertTrue(result.getUpperLimit().center < result.getBottomLimit().center);

        assertTrue(NumericUtils.isNumberAroundValue(result.getLeftLimit().center,
                result.getUpperLimit().center, result.getUpperLimit().tolerance));

        assertTrue(NumericUtils.isNumberAroundValue(result.getRightLimit().center,
                result.getBottomLimit().center, result.getBottomLimit().tolerance * 2));
    }
}
