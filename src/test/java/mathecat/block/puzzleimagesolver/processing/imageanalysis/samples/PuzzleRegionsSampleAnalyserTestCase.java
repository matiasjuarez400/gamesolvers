package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.model.Pair;
import mathecat.block.puzzleimagesolver.model.PositionedPixel;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PuzzleRegionsSampleResult;
import org.junit.Test;

public class PuzzleRegionsSampleAnalyserTestCase extends TestCase {
    private PuzzleRegionsSampleAnalyser analyser = new PuzzleRegionsSampleAnalyser(1);

    @Test
    public void testAnalyse() {
        PuzzleRegionsSampleResult result = analyser.analyse();

        for (Pair<PositionedPixel, PositionedPixel> pair : result.getPixelsAroundUpperRegionLimit()) {
            assertEquals(105, pair.getLeft().getPosition().y);
            assertEquals(107, pair.getRight().getPosition().y);
        }

        for (Pair<PositionedPixel, PositionedPixel> pair : result.getPixelsAroundLowerRegionLimit()) {
            assertEquals(871, pair.getLeft().getPosition().y);
            assertEquals(873, pair.getRight().getPosition().y);
        }

        assertNotNull(result.getBrightnessVariationUpperLimit());
        assertNotNull(result.getBrightnessVariationLowerLimit());
    }
}
