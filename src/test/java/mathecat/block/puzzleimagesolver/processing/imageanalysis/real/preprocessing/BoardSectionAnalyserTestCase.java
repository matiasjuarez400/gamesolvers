package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.utils.ImageIOHandlerForTest;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;

public class BoardSectionAnalyserTestCase extends TestCase {
    private BoardSectionAnalyser analyser;
    private ImageIOHandlerForTest imageIOHandlerForTest;
    private ImageModifier imageModifier;

    @Before
    public void setUp() {
        this.analyser = new BoardSectionAnalyser();
        this.imageIOHandlerForTest = new ImageIOHandlerForTest();
        this.imageModifier = new ImageModifier();
    }

    @Test
    public void testAnalyseBoardRegion() {
        BufferedImage bufferedImage = imageIOHandlerForTest.loadImageByResourceName(TestingValues.BOARD_TEST_SAMPLE_RESOURCE);

        BoardSectionResult result = analyser.analyze(bufferedImage);

        assertNotNull(result);

        imageModifier.drawPointsInImage(result.getOriginalImage(), result.getRightPoints(), false);
        imageModifier.drawPointsInImage(result.getOriginalImage(), result.getDownPoints(), false);
        imageModifier.drawPointsInImage(result.getOriginalImage(), result.getLeftPoints(), false);
        imageModifier.drawPointsInImage(result.getOriginalImage(), result.getUpPoints(), false);

        imageIOHandlerForTest.saveImage(result.getOriginalImage(), BoardSectionAnalyserTestCase.class, "detectedSquarePoints");
    }
}
