package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.model.Line;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PuzzleRegionsResult;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.utils.ImageIOHandlerForTest;
import org.junit.Test;

import java.awt.image.BufferedImage;

public class PuzzleRegionsAnalyserTestCase extends TestCase {
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private ImageIOHandlerForTest imageIOHandlerForTest = new ImageIOHandlerForTest();
    private PuzzleRegionsAnalyser analyser = new PuzzleRegionsAnalyser();

    @Test
    public void testCalculateUpperVerticalCoordinateBoardZone() {
        BufferedImage puzzleImage = imageIOHandler.loadImageByResourceName("/puzzleSample.png");

        PuzzleRegionsResult result = analyser.analyze(puzzleImage);

        assertEquals(105, result.getBoardSectionInitialCoordinate());
        assertEquals(872, result.getPiecesSectionInitialCoordinate());

        int width = result.getOriginalImage().getWidth();

        int boardHeight = result.getBoardSectionImage().getHeight();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < boardHeight; y++) {
                assertEquals(
                        result.getOriginalImage().getRGB(x, y + result.getBoardSectionInitialCoordinate()),
                        result.getBoardSectionImage().getRGB(x, y)
                        );
            }
        }

        int piecesHeight = result.getPiecesSectionImage().getHeight();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < piecesHeight; y++) {
                assertEquals(
                        result.getOriginalImage().getRGB(x, y + result.getPiecesSectionInitialCoordinate()),
                        result.getPiecesSectionImage().getRGB(x, y)
                );
            }
        }

        saveResultImages(result);
    }

    private void saveResultImages(PuzzleRegionsResult result) {
        ImageModifier imageModifier = new ImageModifier();

        BufferedImage copy = imageModifier.copyImage(result.getOriginalImage());

        imageModifier.drawLineInImage(copy,
                new Line(
                        0, result.getBoardSectionInitialCoordinate(),
                        copy.getWidth(), result.getBoardSectionInitialCoordinate()
                ), false);

        imageModifier.drawLineInImage(copy,
                new Line(
                        0, result.getPiecesSectionInitialCoordinate(),
                        copy.getWidth(), result.getPiecesSectionInitialCoordinate()
                ), false);

        imageIOHandlerForTest.saveImage(copy, this.getClass(), "markedRegionsOriginalImage");
        imageIOHandlerForTest.saveImage(result.getBoardSectionImage(), this.getClass(), "boardRegion");
        imageIOHandlerForTest.saveImage(result.getPiecesSectionImage(), this.getClass(), "piecesRegion");

    }
}
