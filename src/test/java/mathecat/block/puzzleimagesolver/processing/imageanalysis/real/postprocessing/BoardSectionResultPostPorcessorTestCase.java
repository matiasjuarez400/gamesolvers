package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.board.BoardSectionMainPostProcessor;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.BoardSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.BoardSectionAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.BoardSectionResult;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.utils.ImageIOHandlerForTest;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class BoardSectionResultPostPorcessorTestCase extends TestCase {
    private BoardSectionMainPostProcessor postProcessor = new BoardSectionMainPostProcessor();
    private ImageIOHandlerForTest imageIOHandler = new ImageIOHandlerForTest();
    private ImageModifier imageModifier = new ImageModifier();
    private BoardSectionAnalyser boardSectionAnalyser = new BoardSectionAnalyser();

    @Test
    public void testAnalyse() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(TestingValues.BOARD_TEST_SAMPLE_RESOURCE);

        BoardSectionResult boardSectionResult = boardSectionAnalyser.analyze(image);

        BoardSectionPostProcessingResult postPorcessingResult = postProcessor.analyse(boardSectionResult);

        assertEquals(7, postPorcessingResult.getModel().length);
        assertEquals(5, postPorcessingResult.getModel()[0].length);

        boolean [][] expectedMatrix = {
                {true, true, true, false, false},
                {true, true, true, true, true},
                {true, true, true, false, true},
                {true, true, true, false, true},
                {true, true, true, true, true},
                {true, true, true, true, true},
                {false, true, true, true, false}
        };

        for (int y = 0; y < postPorcessingResult.getModel().length; y++) {
            for (int x = 0; x < postPorcessingResult.getModel()[0].length; x++) {
                assertEquals(expectedMatrix[y][x], postPorcessingResult.getModel()[y][x]);
            }
        }

        drawResult(postPorcessingResult, "sample1");
    }

    private void drawResult(BoardSectionPostProcessingResult result, String testPrefix) {
        BufferedImage allPointsImage = imageModifier.copyImage(result.getPreResult().getOriginalImage());

        imageModifier.drawPointsInImage(allPointsImage, result.getAllPoints(), false);

        imageIOHandler.saveImage(allPointsImage, this.getClass(), testPrefix + "_ALL");

        BufferedImage upPointsImage = imageModifier.copyImage(result.getPreResult().getOriginalImage());
        imageModifier.drawPointsInImage(upPointsImage, result.getUpPoints(), false);
        imageIOHandler.saveImage(upPointsImage, this.getClass(), testPrefix + "_UP");

        BufferedImage downPointsImage = imageModifier.copyImage(result.getPreResult().getOriginalImage());
        imageModifier.drawPointsInImage(downPointsImage, result.getDownPoints(), false);
        imageIOHandler.saveImage(downPointsImage, this.getClass(), testPrefix + "_DOWN");

        BufferedImage rightPointsImage = imageModifier.copyImage(result.getPreResult().getOriginalImage());
        imageModifier.drawPointsInImage(rightPointsImage, result.getRightPoints(), false);
        imageIOHandler.saveImage(rightPointsImage, this.getClass(), testPrefix + "_RIGHT");

        BufferedImage leftPointsImage = imageModifier.copyImage(result.getPreResult().getOriginalImage());
        imageModifier.drawPointsInImage(leftPointsImage, result.getLeftPoints(), false);
        imageIOHandler.saveImage(leftPointsImage, this.getClass(), testPrefix + "_LEFT");
    }
}
