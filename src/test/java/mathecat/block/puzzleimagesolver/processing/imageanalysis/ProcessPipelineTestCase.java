package mathecat.block.puzzleimagesolver.processing.imageanalysis;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipeline;
import mathecat.block.puzzleimagesolver.processing.pipeline.ProcessPipelineResult;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.resolutionlogic.PuzzleSolutionImageCreator;
import mathecat.block.resolutionlogic.model.Piece;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.List;

public class ProcessPipelineTestCase extends TestCase {
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private ProcessPipeline pipeline;

    @Before
    public void setUp() {
        pipeline = new ProcessPipeline();
    }

    @Test
    public void testAnalyse() {
        BufferedImage puzzleImage = imageIOHandler.loadImageByResourceName("/puzzleSample.png");

        ProcessPipelineResult result = pipeline.analyse(puzzleImage);

        PuzzleSolutionImageCreator puzzleSolutionImageCreator = new PuzzleSolutionImageCreator();
        puzzleSolutionImageCreator.drawSolution(result.getPuzzleSolution());

        System.out.println(result.getPuzzleSolution());
        int a = 0;
    }

    @Test
    public void testProcessPuzzle2() {
        BufferedImage puzzleImage = imageIOHandler.loadImageByResourceName("/puzzleSample.png");

        ProcessPipelineResult result = pipeline.analyse(puzzleImage);

        List<Piece> pieceList = result.getPieceResolutionModelConvertionResult().getPieces();

        assertEquals(8, pieceList.size());
    }
}
