package mathecat.block.puzzleimagesolver.processing.imageanalysis.samples;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.samples.results.PiecesSampleResult;
import org.junit.Test;

public class PiecesSampleAnalyserTestCase extends TestCase {
    private PiecesSampleAnalyser piecesSampleAnalyser = new PiecesSampleAnalyser();

    @Test
    public void testAnalyse() {
        PiecesSampleResult piecesSampleResult = piecesSampleAnalyser.analyse();

        assertNotNull(piecesSampleResult.getGreenColorRange());
        assertNotNull(piecesSampleResult.getLightBlueColorRange());
        assertNotNull(piecesSampleResult.getPinkColorRange());
        assertNotNull(piecesSampleResult.getPurpleColorRange());
        assertNotNull(piecesSampleResult.getRedColorRange());
        assertNotNull(piecesSampleResult.getYellowColorRange());
    }
}
