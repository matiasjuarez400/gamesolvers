package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.model.ImageBlock;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.utils.ImageIOHandlerForTest;
import org.junit.Test;

import java.awt.image.BufferedImage;

import static mathecat.block.TestingValues.PIECES_TEST_SAMPLE_RESOURCE;
import static mathecat.block.TestingValues.PIECES_TEST_SAMPLE_RESOURCE_2;

public class PiecesSectionAnalyserTestCase extends TestCase {
    private PiecesSectionAnalyser analyser = new PiecesSectionAnalyser();
    private ImageIOHandlerForTest imageIOHandler = new ImageIOHandlerForTest();
    private ImageModifier imageModifier = new ImageModifier();

    @Test
    public void testAnalyze_sample1() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(PIECES_TEST_SAMPLE_RESOURCE);

        PiecesSectionResult result = analyser.analyze(image);

        assertNotNull(result);

        assertEquals(29, result.getImageBlocks().size());

        generateImageResult(result, 1);
    }

    @Test
    public void testAnalyze_sample2() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(PIECES_TEST_SAMPLE_RESOURCE_2);

        PiecesSectionResult result = analyser.analyze(image);

        assertNotNull(result);

        assertEquals(31, result.getImageBlocks().size());

        generateImageResult(result, 2);
    }

    private void generateImageResult(PiecesSectionResult result, int sampleNumber) {
        for (ImageBlock imageBlock : result.getImageBlocks()) {
            imageModifier.drawRectangleInImage(result.getOriginalImage(), imageBlock.getRectangle(), false);
        }

        imageIOHandler.saveImage(result.getOriginalImage(), PiecesSectionAnalyserTestCase.class, "detectedPiecesPoints" + sampleNumber);
    }
}
