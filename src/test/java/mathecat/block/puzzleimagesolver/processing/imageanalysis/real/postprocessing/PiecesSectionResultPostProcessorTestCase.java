package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.model.ImageBlock;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PiecesSectionPostProcessingResult;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.postprocessing.results.PostProcessPiece;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.PiecesSectionAnalyser;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing.results.PiecesSectionResult;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import mathecat.block.utils.ImageIOHandlerForTest;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class PiecesSectionResultPostProcessorTestCase extends TestCase {
    private PiecesSectionResultPostProcessor postProcessor = new PiecesSectionResultPostProcessor();
    private ImageIOHandlerForTest imageIOHandler = new ImageIOHandlerForTest();
    private ImageModifier imageModifier = new ImageModifier();
    private PiecesSectionAnalyser piecesSectionAnalyser = new PiecesSectionAnalyser();

    @Test
    public void testAnalyseSample1() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(TestingValues.PIECES_TEST_SAMPLE_RESOURCE);

        PiecesSectionResult preResult = piecesSectionAnalyser.analyze(image);

        PiecesSectionPostProcessingResult postResult = postProcessor.analyse(preResult);

        drawPieces(postResult, "sample_1_piece_");

        assertEquals(8, postResult.getPostProcessPieces().size());

        Map<Integer, Integer> pieceSizeCounter = new HashMap<>();
        int totalPieces = 0;
        for (PostProcessPiece postProcessPiece : postResult.getPostProcessPieces()) {
            int piecesBlockSize = postProcessPiece.getBlocks().size();
            totalPieces += piecesBlockSize;

            Integer currentCounter = pieceSizeCounter.get(piecesBlockSize);

            if (currentCounter == null) {
                currentCounter = 1;
            } else {
                currentCounter++;
            }

            pieceSizeCounter.put(piecesBlockSize, currentCounter);
        }

        assertEquals(29, totalPieces);

        assertEquals(5, (int) pieceSizeCounter.get(4));
        assertEquals(1, (int) pieceSizeCounter.get(3));
        assertEquals(1, (int) pieceSizeCounter.get(1));
        assertEquals(1, (int) pieceSizeCounter.get(5));
    }

    @Test
    public void testAnalyseSample2() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(TestingValues.PIECES_TEST_SAMPLE_RESOURCE_2);

        PiecesSectionResult preResult = piecesSectionAnalyser.analyze(image);

        PiecesSectionPostProcessingResult postResult = postProcessor.analyse(preResult);

        drawPieces(postResult, "sample_2_piece_");

        assertEquals(8, postResult.getPostProcessPieces().size());

        Map<Integer, Integer> pieceSizeCounter = new HashMap<>();
        int totalPieces = 0;
        for (PostProcessPiece postProcessPiece : postResult.getPostProcessPieces()) {
            int piecesBlockSize = postProcessPiece.getBlocks().size();
            totalPieces += piecesBlockSize;

            Integer currentCounter = pieceSizeCounter.get(piecesBlockSize);

            if (currentCounter == null) {
                currentCounter = 1;
            } else {
                currentCounter++;
            }

            pieceSizeCounter.put(piecesBlockSize, currentCounter);
        }

        assertEquals(31, totalPieces);

        assertEquals(5, (int) pieceSizeCounter.get(4));
        assertEquals(2, (int) pieceSizeCounter.get(3));
        assertEquals(1, (int) pieceSizeCounter.get(5));
    }

    private void drawPieces(PiecesSectionPostProcessingResult postResult, String testPrefix) {
        int pieceCounter = 1;
        for (PostProcessPiece postProcessPiece : postResult.getPostProcessPieces()) {
            BufferedImage copy = imageModifier.copyImage(postResult.getPreResult().getOriginalImage());
            for (ImageBlock imageBlock : postProcessPiece.getBlocks()) {
                imageModifier.drawRectangleInImage(copy, imageBlock.getRectangle(), false);
            }

            imageIOHandler.saveImage(copy, PiecesSectionResultPostProcessorTestCase.class, testPrefix + pieceCounter);
            pieceCounter++;
        }
    }
}
