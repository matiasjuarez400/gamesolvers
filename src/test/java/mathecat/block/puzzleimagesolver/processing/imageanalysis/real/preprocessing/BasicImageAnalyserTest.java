package mathecat.block.puzzleimagesolver.processing.imageanalysis.real.preprocessing;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.resolutionlogic.model.Point;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.BasicImageAnalyser;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;
import mathecat.block.puzzleimagesolver.processing.utils.NumericUtils;
import mathecat.block.puzzleimagesolver.utils.ImageIOHandler;
import mathecat.block.puzzleimagesolver.utils.ImageModifier;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

public class BasicImageAnalyserTest extends TestCase {
    private ImageModifier imageModifier = new ImageModifier();
    private BasicImageAnalyser basicImageAnalyser = new BasicImageAnalyser();
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private TotalLumaPixelCalculationsHelper totalLumaPixelCalculationsHelper = TotalLumaPixelCalculationsHelper.getInstance();
    private int pixelsTogether = 2;

    @Test
    public void testFindZonesWithBrightnessVariationInRange() {
        BufferedImage barImage = imageIOHandler.loadImageByResourceName(TestingValues.BAR_IMAGE_RESOURCE);
        barImage = barImage.getSubimage(0, 0, 10, barImage.getHeight());

        double desiredBrightnessVariation = totalLumaPixelCalculationsHelper.calculateBrightnessVariation(
                TestingValues.BAR_IMAGE_DARK_PIXEL, TestingValues.BAR_IMAGE_LIGHT_PIXEL
        );
        double tolerance = 0.0001;

        List<Point> points = basicImageAnalyser.findZonesWithBrightnessVariation(barImage, desiredBrightnessVariation,
                tolerance, pixelsTogether, BasicImageAnalyser.SearchDirection.VERTICAL);

        assertFalse(points.isEmpty());
        assertEquals(40, points.size());

        List<Integer> yLocations = Arrays.asList(25, 66, 117, 159);
        int yDifferenceTolerance = 0;

        for (Point point : points) {
            boolean isInRightLocation = false;
            for (Integer location : yLocations) {
                if (NumericUtils.isNumberAroundValue(point.y, location, yDifferenceTolerance)) {
                    isInRightLocation = true;
                    break;
                }
            }

            if (!isInRightLocation) {
                fail("There is a point that marked in the image that should not exist");
            }
        }

        imageIOHandler.saveImage(imageModifier.drawPointsInImage(barImage, points), TestingValues.TEST_TEMP_FOLDER, "barImageWithPoints");
    }
}
