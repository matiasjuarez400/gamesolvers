package mathecat.block.puzzleimagesolver.utils;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.processing.imageanalysis.BasicImageAnalyser;
import org.junit.Test;

import java.awt.image.BufferedImage;

public class ImageModifierTest extends TestCase {
    private ImageIOHandler imageIOHandler = new ImageIOHandler();
    private ImageModifier imageModifier = new ImageModifier();
    private BasicImageAnalyser basicImageAnalyser = new BasicImageAnalyser();

    @Test
    public void testCopyImage() {
        BufferedImage imageBar = imageIOHandler.loadImageByResourceName(TestingValues.BAR_IMAGE_RESOURCE);
        int[][] imageBarPixels = basicImageAnalyser.extractPixelsMatrix(imageBar);

        BufferedImage imageCopy = imageModifier.copyImage(imageBar);
        int[][] imageCopyPixels = basicImageAnalyser.extractPixelsMatrix(imageCopy);

        assertNotNull(imageCopy);
        assertEquals(imageBar.getWidth(), imageCopy.getWidth());
        assertEquals(imageBar.getHeight(), imageCopy.getHeight());

        for (int y = 0; y < imageBar.getHeight(); y++) {
            for (int x = 0; x < imageBar.getWidth(); x++) {
                assertEquals(imageBar.getRGB(x, y), imageCopy.getRGB(x, y));
                assertEquals(imageBar.getRGB(x, y), imageBarPixels[y][x]);
                assertEquals(imageBar.getRGB(x, y), imageCopyPixels[y][x]);
            }
        }
    }
}
