package mathecat.block.puzzleimagesolver.utils;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import mathecat.block.puzzleimagesolver.model.Pixel;
import mathecat.block.puzzleimagesolver.model.Range;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.TotalLumaPixelCalculationsHelper;
import mathecat.block.puzzleimagesolver.processing.utils.BrigthnessVariationRangeCalculator;
import org.junit.Test;

public class BrigthnessVariationRangeCalculatorTest extends TestCase {
    private TotalLumaPixelCalculationsHelper helper = TotalLumaPixelCalculationsHelper.getInstance();

    @Test
    public void testCalculate_1Entry() {
        BrigthnessVariationRangeCalculator brigthnessVariationRangeCalculator = new BrigthnessVariationRangeCalculator();

        brigthnessVariationRangeCalculator.addEntry(TestingValues.BAR_IMAGE_DARK_PIXEL, TestingValues.BAR_IMAGE_LIGHT_PIXEL);

        Range range = brigthnessVariationRangeCalculator.calculate();

        double darkLuma = helper.calculateBrightness(TestingValues.BAR_IMAGE_DARK_PIXEL);
        double brightLuma = helper.calculateBrightness(TestingValues.BAR_IMAGE_LIGHT_PIXEL);

        double brightnessDiference = (brightLuma - darkLuma) / (brightLuma + darkLuma);
        double delta = 0.00001;

        assertEquals(brightnessDiference, range.center, delta);
        assertEquals(0, range.tolerance, delta);
        assertEquals(brightnessDiference, range.lowerLimit, delta);
        assertEquals(brightnessDiference, range.upperLimit, delta);
    }

    @Test
    public void testCalculate_3Entries() {
        BrigthnessVariationRangeCalculator brigthnessVariationRangeCalculator = new BrigthnessVariationRangeCalculator();

        Pixel p1 = new Pixel(38, 69, 100);
        Pixel p2 = new Pixel(64, 115, 167);
        double lumaVariation1 = helper.calculateBrightnessVariation(p1, p2);

        Pixel p3 = new Pixel(32, 64, 97);
        Pixel p4 = new Pixel(51, 106, 162);
        double lumaVariation2 = helper.calculateBrightnessVariation(p3, p4);

        Pixel p5 = new Pixel(49, 77, 106);
        Pixel p6 = new Pixel(81, 128, 175);
        double lumaVariation3 = helper.calculateBrightnessVariation(p5, p6);

        brigthnessVariationRangeCalculator.addEntry(p1, p2);
        brigthnessVariationRangeCalculator.addEntry(p3, p4);
        brigthnessVariationRangeCalculator.addEntry(p5, p6);

        Range range = brigthnessVariationRangeCalculator.calculate();

        assertEquals(lumaVariation2, range.lowerLimit);
        assertEquals(lumaVariation1, range.upperLimit);

        double middleValue = (lumaVariation1 + lumaVariation2) / 2.0;

        assertEquals(middleValue, range.center);
        assertEquals(lumaVariation1 - middleValue, range.tolerance);
    }
}
