package mathecat.block.puzzleimagesolver.utils;

import junit.framework.TestCase;
import mathecat.block.TestingValues;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.File;

public class ImageIOHandlerTest extends TestCase {
    private ImageIOHandler imageIOHandler = new ImageIOHandler();

    @Test
    public void testThatSavedImageIsEqualToOriginalImage() {
        BufferedImage image = imageIOHandler.loadImageByResourceName(TestingValues.BAR_IMAGE_RESOURCE);

        String savingName = "imageIoSavingImageTest";

        imageIOHandler.saveImage(image, TestingValues.TEST_TEMP_FOLDER, savingName);

        String retrievingImageUrl = TestingValues.TEST_TEMP_FOLDER + File.separator + savingName + ImageIOHandler.DEFAULT_EXTENSION;

        BufferedImage retrievedImage = imageIOHandler.loadImageByUrl(retrievingImageUrl);

        assertNotNull(retrievedImage);
        assertEquals(image.getWidth(), retrievedImage.getWidth());
        assertEquals(image.getHeight(), retrievedImage.getHeight());

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                assertEquals(image.getRGB(x, y), retrievedImage.getRGB(x, y));
            }
        }
    }
}
