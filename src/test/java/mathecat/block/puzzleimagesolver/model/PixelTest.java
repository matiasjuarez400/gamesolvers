package mathecat.block.puzzleimagesolver.model;

import junit.framework.TestCase;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.BrightnessCalculator;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.LumaDifferenceCalculator;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.LumaProportionCalculator;
import mathecat.block.puzzleimagesolver.processing.pixelcalculations.brightnessvariationcalculation.TotalLumaProportionCalculator;
import org.junit.Test;

public class PixelTest extends TestCase {
    private Pixel pixel1 = new Pixel(26,87,145);
    private Pixel pixel2 = new Pixel(7,45,81);

    private Pixel pixel3 = new Pixel(22,84,143);
    private Pixel pixel4 = new Pixel(10,44,81);

    private Pixel pixel5 = new Pixel(26,83,147);
    private Pixel pixel6 = new Pixel(7,45,81);

    private Pixel pixel7 = new Pixel(0, 0, 0);
    private Pixel pixel8 = new Pixel(0, 0, 0);
    private Pixel pixel9 = new Pixel(0, 0, 1);
    private Pixel pixel10 = new Pixel(255, 255, 255);

    private double delta = 0.0001;
    
    @Test
    public void testPixelDistanceCalculation() {
        assertEquals(78.8733, pixel1.calculateColorDistance(pixel2), delta);
        assertEquals(74.7529, pixel3.calculateColorDistance(pixel4), delta);
        assertEquals(78.4920, pixel5.calculateColorDistance(pixel6), delta);
        assertEquals(0.0, pixel7.calculateColorDistance(pixel8), delta);
        assertEquals(441.6729, pixel8.calculateColorDistance(pixel10), delta);
    }

    @Test
    public void testCalculateBrightnessProportionVariationWithLuma() {
        new CalculatorPrinter() {
            @Override
            double doCalculation(Pixel p1, Pixel p2) {
                return p1.calculateBrightnessProportionVariationWithLuma(p2);
            }
        }.print();

        assertEquals(-0.49926, pixel1.calculateBrightnessProportionVariationWithLuma(pixel2), delta);
        assertEquals(-0.47287, pixel3.calculateBrightnessProportionVariationWithLuma(pixel4), delta);
        assertEquals(-0.48477, pixel5.calculateBrightnessProportionVariationWithLuma(pixel6), delta);
        assertEquals(-1.0, pixel6.calculateBrightnessProportionVariationWithLuma(pixel8));
        assertEquals(-1.0, pixel10.calculateBrightnessProportionVariationWithLuma(pixel8));

        assertEquals(0.997058, pixel2.calculateBrightnessProportionVariationWithLuma(pixel1), delta);
        assertEquals(0.897088, pixel4.calculateBrightnessProportionVariationWithLuma(pixel3), delta);
        assertEquals(0.940888, pixel6.calculateBrightnessProportionVariationWithLuma(pixel5), delta);
        assertTrue(Double.isInfinite(pixel8.calculateBrightnessProportionVariationWithLuma(pixel6)));
        assertEquals(2235.8421, pixel9.calculateBrightnessProportionVariationWithLuma(pixel10), delta);
        assertEquals(0.0, pixel9.calculateBrightnessProportionVariationWithLuma(pixel9), delta);
    }

    @Test
    public void testCalculationStrategies() {
        System.out.println("LUMA DIFFERENCE");
        CalculatorStrategyPrinter lumaDifference = new CalculatorStrategyPrinter(new LumaDifferenceCalculator());

        lumaDifference.print();

        System.out.println("LUMA PROPORTION VARIATION");
        CalculatorStrategyPrinter lumaProportionalVariation = new CalculatorStrategyPrinter(new LumaProportionCalculator());

        lumaProportionalVariation.print();

        System.out.println("LUMA TOTAL PROPORTION VARIATION");
        CalculatorStrategyPrinter lumaTotalProportionalVariation = new CalculatorStrategyPrinter(new TotalLumaProportionCalculator());

        lumaTotalProportionalVariation.print();
    }

    @Test
    public void testCalculateBrightnessDifferenceWithLuma() {
        new BrightnessDiffrenceWithLumaCalculator().print();
    }

    public void testInitializeWithHex() {
        String hex = "#ff23a2";
        Pixel pixel = new Pixel(hex);

        int a = 0;
    }

    private class BrightnessDiffrenceWithLumaCalculator extends CalculatorPrinter {
        @Override
        double doCalculation(Pixel p1, Pixel p2) {
            return p1.calculateBrightnessDifferenceWithLuma(p2);
        }
    }

    private abstract class CalculatorPrinter {
        abstract double doCalculation(Pixel p1, Pixel p2);

        public void print() {
            System.out.println(String.format(
                    "dark1: %s. dark2: %s. dark3: %s \n" +
                            "light1: %s. light2: %s. light3: %s",
                    doCalculation(pixel1, pixel2),
                    doCalculation(pixel3, pixel4),
                    doCalculation(pixel10, pixel9),
                    doCalculation(pixel2, pixel1),
                    doCalculation(pixel4, pixel3),
                    doCalculation(pixel9, pixel10)
            ));
        }
    }

    private class CalculatorStrategyPrinter {
        private BrightnessCalculator brightnessCalculator;
        public CalculatorStrategyPrinter(BrightnessCalculator brightnessCalculator) {
            this.brightnessCalculator = brightnessCalculator;
        }

        public double doCalculation(Pixel p1, Pixel p2) {
            return brightnessCalculator.calculateBrightnessVariation(p1, p2);
        }

        public void print() {
            System.out.println(String.format(
                    "dark1: %s. dark2: %s. dark3: %s dark4: %s\n" +
                            "light1: %s. light2: %s. light3: %s",
                    doCalculation(pixel1, pixel2),
                    doCalculation(pixel3, pixel4),
                    doCalculation(pixel5, pixel6),
                    doCalculation(pixel10, pixel9),
                    doCalculation(pixel2, pixel1),
                    doCalculation(pixel4, pixel3),
                    doCalculation(pixel9, pixel10)
            ));
        }
    }
}
