package mathecat.block;

import mathecat.block.puzzleimagesolver.model.Pixel;

public class TestingValues {
    public static final String TEST_TEMP_FOLDER = "D:\\Documents\\Projects\\problemsolver\\src\\test\\testTempFolder";
    public static final String BAR_IMAGE_RESOURCE = "/testingBar.png";
    public static final Pixel BAR_IMAGE_DARK_PIXEL = new Pixel(34, 70, 104);
    public static final Pixel BAR_IMAGE_LIGHT_PIXEL = new Pixel(67, 115, 164);

    public static final String BOARD_TEST_SAMPLE_RESOURCE = "/boardRegionSample.png";
    public static final String PIECES_TEST_SAMPLE_RESOURCE = "/piecesRegionSample1.png";
    public static final String PIECES_TEST_SAMPLE_RESOURCE_2 = "/piecesRegionSample2.png";
}
